# Praktikum Modul 3 Sistem Operasi

Perkenalkan kami dari kelas ``Sistem Operasi B Kelompok  B08``, dengan Anggota sebagai berikut:

| Nama                      | NRP        |
|---------------------------|------------|
|Dimas Prihady Setyawan     | 5025211184 |
|Yusuf Hasan Nazila         | 5025211225 |
|Ahda Filza Ghaffaru        | 5025211144 |

# Penjelasan soal nomor 1
Untuk soal nomor 1, kita diminta untuk melakukan kompresi terhadap suatu kalimat di dalam ``file.txt``yang telah diberikan di google drive. Nantinya, proses kompresi akan dibantu dengan **Algoritma Huffman**. Referensi dari Algoritma: ``https://www.geeksforgeeks.org/huffman-coding-greedy-algo-3/``

## Penjelasan Soal 1A
Untuk poin soal A, pada **Parent Process**, kita diperintah untuk membuka / __read__ ``file.txt`` dilanjutkan dengan menghitung frekuensi setiap huruf yang muncul pada kalimat di dalam file tersebut. 

1. Pada fungsi ``main()``, awalnya kita perlu __declare__ variabel dari 2 pipe yang mana pipe pertama (``fd1``) akan digunakan untuk mengirim data dari parent ke child, dan pipe kedua (``fd2``) akan mengirim data dari child ke parent.
2. Setelah itu, baru kita lakukan proses **forking** untuk memunculkan 2 proses yaitu parent dan juga childnya.

Kurang lebih, potongan kode-nya sebagaimana berikut.
```c
int fd1[2]; // Used to store two ends of first pipe 
int fd2[2]; // Used to store two ends of second pipe 

pid_t p; 

if (pipe(fd1)==-1) 
{ 
    fprintf(stderr, "Pipe Failed" ); 
    return 1; 
} 
if (pipe(fd2)==-1) 
{ 
    fprintf(stderr, "Pipe Failed" ); 
    return 1; 
} 

p = fork(); 

if (p < 0) 
{ 
    fprintf(stderr, "fork Failed" ); 
    return 1; 
} 
```

3. Selanjutnya, kita akan membuka ``file.txt`` melalui fungsi ``fopen()`` sebagaimana berikut.
```c
FILE *fpin;
char input[10] = "file.txt"; 

if((fpin = fopen(input, "r")) == NULL){
    printf("Unable to open");
    exit(0);
}
```

4. Dilanjutkan dengan menyalin kalimat yang ada didalam variabel ``fpin`` ke dalam variabel ``string``. Proses copy kalimat dimulai dengan mengambil karakter pertama ``ch`` dilanjut dengan menjalankan ``while(ch!=EOF)`` yang berarti selama karakter belum sampai akhir maka lakukan pengecekan -> pengambilan karakter yang hanya alfabet dan juga spasi, lalu ubah mereka menjadi huruf kapital dan tempel karakter tersebut ke variabel ``string`` melalui fungsi ``strncat()``. Jika sudah berhenti, maka kita akan melakukan backup string tersebut karena kita akan melakukan perubahan terhadap string.
```c
char string[1000];
char ch = getc(fpin); 
while ((ch!=EOF)){ // belum sampai endfile
    if(((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' ')){ 
        char save[2]; // simpan karakter pada array of string, karena nanti mau di strcat ke variabel string.
        save[0] = toupper(ch); // Berdasarkan soal, ubah semua huruf ke kapital
        strncat(string, save, 1); // tempel char tersebut ke variabel string.
    }
    ch = getc(fpin); // lanjut karakter berikutnya.
}
```

5. Tahap selanjutnya yaitu menghitung frekuensi munculnya tiap huruf, untuk metode-nya adalah dengan mengambil karakter pertama dan membandingkannya dengan index-index berikutnya. Nantinya tiap distinct letters dan frekuensinya akan disimpan dalam array of char dan juga array of integer yaitu variabel ``letters[24]`` dan ``occur[24]``. Kedua array tersebut akan dibutuhkan dalam proses kompresi dengan **Algoritma Huffman**.
```c
for (int i = 0; i < len; i++){
    count = 1; // starting point huruf sudah muncul sekali.
    if (string[i] != '\0'){ // jika karakter bukan \0 -> semacam marking huruf
        for (int j = i+1; j < len; j++){
            if (string[i] == string[j]){ // jika karakter awal = yang sedang diperiksa 
                count++;
                string[j] = '\0'; // mark huruf dengan \0, sehingga pada pengecekan huruf berikutnya, huruf ini langsung di skip.
            }
        }
        
        letters[temp] = string[i]; // simpan distinct letters ke array letters
        occur[temp] = count; // simpan berapa kali huruf tersebut muncul ke array occur
        temp++; // increment index
    }
}
```

6. Terakhir, kita akan mengirim data-data yang diperlukan ke child process yaitu array ``letters``, ``occur``, dan juga struct ``batch`` yang menyimpan hasil backup ``string`` diawal.
```c
write(fd1[1], &n, sizeof(int));
write(fd1[1], letters, sizeof(char)*n); 
write(fd1[1], occur, sizeof(int)*n);
write(fd1[1], &batch, sizeof(struct parentToChild));
```

## Penjelasan Soal 1B - 1C
Untuk section soal 1B dan 1C, kita perlu melakukan proses kompresi **Huffman** dengan menggunakan data-data yang dikirim melalui **Parent Process** tadi. 

1. Setelah melakukan ``Read()`` data-data dari parent, simpelnya kita langsung melakukan pembuatan **Tree Huffman** dengan memanggil fungsi ``BuildHuffmanTree(letters, occur, n)``. Fungsi ini adalah bawaan dari referensi yang saya gunakan.
```c
root = buildHuffmanTree(letters, occur, n); // buat tree huffman dengan mengirim array yang diterima dari parent. 
```
2. Jika tree sudah jadi, langkah berikutnya adalah mengubah data-data yang ada di dalam tree kedalam bentuk **Array** dengan nama variabel ``compressedLetter[]`` dan juga ``binaryVal[]``. Sehingga, array tsb nanti bisa membantu kita untuk melakukan kompresi ``string`` awal serta dikirim kembali ke **Parent Process** untuk dilakukan proses dekompresi. Proses konversi ke array dibantu dengan memanggil fungsi ``PrintCodes()`` dari referensi yang telah saya modifikasi. Kurang lebih potongan kode-nya sebagaimana berikut.
```c
printCodes(root, arr, top, compressedLetter, binaryVal);
if (isLeaf(root)) { // kalau sudah sampai leaf
    letters[idx] = root->data; // data huruf nya disimpan pada array letters.
    char conv[2]; // array char untuk convert kode biner yang masih dalam bentuk int ke char
    
    for (int i = 0; i < top; i++){ // untuk setiap digit pada kode biner huruf tersebut,
        sprintf(conv, "%d", arr[i]); // convert ke char
        binaryVal[idx][i] = conv[0]; // simpan ke array char 2D -> misal ingin akses kode biner untuk huruf index 0, maka akses binaryVal[0].
    }

    binaryVal[idx][top] = '\0'; // mark akhir array of char nya.
    idx++; // lanjut index huruf dan biner selanjutnya.
}
```

3. Berikutnya, kita akan melakukan kompresi dari string awal dengan cara membandingkan setiap karakternya dengan array yang sudah kita dapat tadi yaitu ``compressedLetter`` dan juga kode binernya / ``binaryVal[]``.
```c
// dari array huffman yang dimiliki, periksa setiap huruf pada kalimat di file, ubah ke kode binary.
for (int i = 0; i < batch.strlen; i++){
    char cek = batch.string[i]; // ambil index ke i, simpan di char.
    for (int j = 0; j < n; j++){ // pengecekan array huffman
        if (cek == compressedLetter[j]){ // jika huruf sama dengan distinct letter pada array
            strcpy(batch2.compressedString[i], binaryVal[j]); // ambil kode binernya, copy ke compressed string.
        }
    }
}
```

4. Terakhir, karena kita sudah memiliki ``string`` yang terkompresi, maka kita akan mengirim data-data yang diperlukan ke **Parent Process** untuk dilakukan proses dekompresi.
```c
write(fd2[1], compressedLetter, sizeof(char)*n);
write(fd2[1], binaryVal, sizeof(char)*n*9);
write(fd2[1], &batch2, sizeof(struct childToParent)); // batch menyimpan string yang terkompresi
```

## Penjelasan Section Soal 1D - 1E
Pada section soal terakhir ini, kita kembali ke **Parent Process** untuk melakukan dekompresi dari ``string`` yang sudah terkompresi. Dilanjutkan dengan proses perbandingan jumlah bit pada ``string`` awal dan juga ``string`` setelah terkompresi -> ``bitBefore`` dan ``bitAfter``

1. Hal pertama yang perlu dilakukan tentu adalah melakukan ``read()`` dari data-data yang dikirim oleh child, kemudian kita akan membandingkan seluruh kode biner tiap huruf pada ``batch2.compressedString[]`` terhadap array ``compressedLetter[]`` dan array ``binaryVal[]`` melalui perulangan berikut.
```c
for (int i = 0; i < batch2.strlen; i++){
    char binary[9]; // menyimpan kode biner untuk tiap huruf
    strcpy(binary, batch2.compressedString[i]); // copy biner untuk huruf index ke i ke variabel
    for (int j = 0; j < 24; j++){
        if (strcmp(binary, binaryVal[j]) == 0){ // compare biner huruf yang sedang di cek dengan biner-biner yang ada di array
            decompressed[i] = compressedLetter[j]; // set string index ke-i dengan huruf yang sesuai dengan binernya.
            bitAfter += strlen(binaryVal[j]); // banyak digit binernya / panjang string nya ditambahkan ke variabel bitAfter
        }
    }
}
```
Dapat dilihat, saat proses dekompresi, kita sekaligus menghitung value dari ``bitAfter`` yaitu jumlah digit dari tiap kode biner hurufnya / ``strlen()`` nya.

2. Terakhir, kita hanya perlu menghitung bit sebelum dikompresi. Secara simpel, kita hanya perlu mengalikan panjang string awal dengan 8 -> setiap huruf alfabet biasa berukuran 8 bit.
```c
bitBefore = strlen(decompressed) * 8; // bit pada setiap huruf alfabet bernilai 8.
```

Sekian penjelasan dari nomor 1.
# Penjelasan Soal Nomor 2

Soal nomor 2 meminta untuk membuat 3 program dengan tujuannya sebagai berikut:
1. Membuat program C dengan nama ``kalian.c`` yang melakukan perkalian matriks. Ukuran matriks pertama adalah ``4 x 2`` dengan berisi angka acak ``1-5``(inklusif) dan Ukuran matriks kedua adalah ``2 x 5`` dengan berisi angka acak ``1-4``(inklusif). Terakhir tampilkan hasil perkalian matriks ke layar

## Penjelasan Program ``kalian.c`` (Nomor 1)
#### 1. Pengunaan Library dan Define
```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4   
#define COL1 2   
#define ROW2 2   
#define COL2 5   
#define SHM_SIZE 1024  
```
Pada program ``kalian.c`` ada beberapa hal yang harus disiapkan dimulai dari library dan define. 
Berikut adalah penjelasan library yang digunakan:

1. ``stdio.h``: library standar input/output di C yang memungkinkan program untuk membaca dan menulis data dari dan ke layar atau file.

2. ``stdlib.h``: library standar yang berisi fungsi-fungsi umum seperti pengalokasian memori, pengubahan tipe data, pengurutan data, dan fungsi matematika.

3. ``time.h``: library standar yang memungkinkan program untuk melakukan manipulasi waktu, seperti mendapatkan waktu saat ini, mengubah format waktu, atau menghitung durasi antara waktu yang berbeda.

4. ``unistd.h``: library standar yang berisi fungsi-fungsi sistem yang digunakan untuk mengakses fungsi-fungsi yang berkaitan dengan sistem operasi.

5. ``sys/ipc.h``: library yang berisi fungsi-fungsi sistem yang digunakan untuk mengakses Shared Memory.

6. ``sys/shm.h``: library yang berisi definisi dan fungsi-fungsi untuk mengelola Shared Memory.

Setelah library dibuat, define dilakukan:

1. ``#define ROW1 4``, ``#define COL1 2``, ``#define ROW2 2``, dan ``#define COL2 5``: Digunakan untuk mendefinisikan konstanta pada program. Dalam kasus ini, konstanta digunakan untuk menentukan ukuran matriks yang akan digunakan dalam program.

2. ``#define SHM_SIZE 1024``: Digunakan untuk mendefinisikan ukuran Shared Memory yang akan digunakan dalam program.

#### 2. Penjelasan Fungsi ``multiply_matrices``

```c
void multiply_matrices(unsigned long long mat1[][COL1], unsigned long long mat2[][COL2], unsigned long long result[][COL2]) {
    int i, j, k;
    for (i = 0; i < ROW1; i++) {     
        for (j = 0; j < COL2; j++) { 
            result[i][j] = 0;       
            for (k = 0; k < ROW2; k++) { 
                result[i][j] += mat1[i][k] * mat2[k][j]; 
            }
        }
    }
}
```
Fungsi ``multiply_matrices`` merupakan implementasi dari operasi perkalian matriks pada program C. Fungsi ini menerima tiga parameter yaitu dua buah matriks yaitu ``mat1`` dan ``mat2`` yang akan dikalikan, dan sebuah matriks hasil yaitu ``result`` yang akan menyimpan hasil perkalian dari kedua matriks tersebut.

Fungsi ini bekerja dengan menggunakan tiga buah nested loop, dimana loop pertama (i) dan loop kedua (j) digunakan untuk mengakses baris dan kolom pada matriks hasil ``result``, sedangkan loop ketiga (k) digunakan untuk mengalikan elemen pada matriks pertama ``mat1`` dan matriks kedua ``mat2`` lalu mengakumulasikan hasil perkalian tersebut pada elemen matriks hasil.

Dalam setiap iterasi, fungsi ini akan mengalikan elemen pada baris ke-i dan kolom ke-j pada matriks hasil ``result`` dengan melakukan operasi penjumlahan dari perkalian elemen pada baris ke-i dan kolom ke-k pada matriks pertama ``mat1`` dengan elemen pada baris ke-k dan kolom ke-j pada matriks kedua ``mat2``. Hasil dari operasi perkalian tersebut akan diakumulasikan pada elemen hasil pada baris ke-i dan kolom ke-j.

#### 3. Penjelasan Fungsi ``print_matrix``

```c

void print_matrix(unsigned long long mat[][COL2], int row, int col) {
    int i, j;
    for (i = 0; i < row; i++) {     
        for (j = 0; j < col; j++) { 
            printf("%llu ", mat[i][j]); 
        }
        printf("\n"); 
    }
}
```
Fungsi ``print_matrix`` digunakan untuk mencetak isi dari sebuah matriks dengan format tertentu. Fungsi ini memiliki tiga parameter, yaitu ``mat`` yang merupakan matriks yang ingin dicetak, ``row`` yang merupakan jumlah baris dari matriks, dan ``co``l yang merupakan jumlah kolom dari matriks.

Fungsi ini akan melakukan iterasi sebanyak ``row`` kali dan untuk setiap baris akan melakukan iterasi sebanyak ``col`` kali untuk mencetak setiap elemen matriks. Setiap elemen matriks yang dicetak akan diikuti dengan satu spasi. Setelah selesai mencetak satu baris, fungsi ini akan memindahkan kursor ke baris baru dengan menggunakan ``printf("\n")``.

#### 4. Penjelasan Fungsi main

```c
int main() {
    unsigned long long mat1[ROW1][COL1], mat2[ROW2][COL2], result[ROW1][COL2];
    int i, j;
    // key 1 for cinta.c
    key_t key1 = 1234; 
    // key 2 for sisop.c
    key_t key2 = 5678; 

    // Initialize random seed
    srand(time(NULL));

```
Pada kode ``int main()``, program menginisialisasi beberapa variabel seperti ``mat1, mat2, dan result`` sebagai ``matriks berukuran 4x2, 2x5, dan 4x5,`` masing-masing.

Selanjutnya, program juga mendefinisikan dua variabel tipe ``key_t`` yaitu ``key1`` dengan nilai 1234, dan ``key2`` dengan nilai 5678. Kedua variabel ini akan digunakan dalam konsep shared memory pada program ``cinta.c`` dan ``sisop.c``.

Kemudian, program melakukan inisialisasi seed untuk fungsi ``rand()`` dengan menggunakan ``srand(time(NULL))``. Hal ini berguna agar program menghasilkan nilai acak yang berbeda-beda setiap kali program dijalankan.

```c
    // Generate random values for mat1
    printf("Matrix 1:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            mat1[i][j] = rand() % 5 + 1;
            printf("%llu ", mat1[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    // Generate random values for mat2
    printf("Matrix 2:\n");
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            mat2[i][j] = rand() % 4 + 1;
            printf("%llu ", mat2[i][j]);
        }
        printf("\n");
    }

    printf("\n");
```
Selanjutnya pada fungsi ``main``, terdapat kode untuk menghasilkan matriks acak untuk ``mat1`` dan ``mat2`` menggunakan fungsi ``rand()`` yang akan menghasilkan nilai acak antara 1 dan 5 untuk ``mat1``, dan antara 1 dan 4 untuk ``mat2``. 
```c

    // Perform matrix multiplication
    multiply_matrices(mat1, mat2, result);

    // Print the resulting matrix
    printf("Result of matrix multiplication:\n");
    print_matrix(result, ROW1, COL2);

```
Kemudian, dilakukan perkalian matriks antara ``mat1`` dan ``mat2`` dengan memanggil fungsi ``multiply_matrices()`` yang sudah didefinisikan sebelumnya dan menyimpan hasilnya ke dalam ``result``. Setelah itu, hasil perkalian matriks akan ditampilkan ke layar menggunakan fungsi ``print_matrix()`` dengan parameter ``result``, ``ROW1``, dan ``COL2``.

```c

    // create shared memory
    int shmid = shmget(key1, SHM_SIZE, 0666 | IPC_CREAT);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // create shared memory key2
    int shmid2 = shmget(key2, SHM_SIZE, 0666 | IPC_CREAT);
    if (shmid2 < 0) {
        perror("shmget");
        exit(1);
}
```
Pada bagian ini, program akan melakukan operasi terhadap shared memory. Pertama-tama, program akan membuat shared memory dengan menggunakan ``key1 dan key2`` menggunakan fungsi ``shmget`` dengan ukuran ``SHM_SIZE``. Jika operasi membuat shared memory tidak berhasil, program akan menampilkan pesan error dan keluar dari program.

```c

    // attach shared memory
    unsigned long long *shm_ptr1 = (unsigned long long *)shmat(shmid, NULL, 0);
    if ((intptr_t)shm_ptr1 == -1) {
        perror("shmat");
        exit(1);
    }

    // attach shared memory key2
    unsigned long long *shm_ptr2 = (unsigned long long *)shmat(shmid2, NULL, 0);
    if ((intptr_t)shm_ptr2 == -1) {
        perror("shmat");
        exit(1);
}
```
Setelah shared memory berhasil dibuat, program akan meng-attach shared memory menggunakan fungsi ``shmat`` untuk ``key1`` dan ``key2``. Jika operasi attach shared memory tidak berhasil, program akan menampilkan pesan error dan keluar dari program.
```c

    // copy result matrix to shared memory
    unsigned long long *result_ptr = &result[0][0];
    for (i = 0; i < ROW1 * COL2; i++) {
        shm_ptr1[i] = result_ptr[i];
        shm_ptr2[i] = result_ptr[i];
    }

printf("Result matrix has been written to shared memory with keys %d and %d\n", key1, key2);
```
Setelah shared memory berhasil di-attach, program akan menyalin isi dari result matrix ke dalam shared memory yang telah dibuat. Penyalinan dilakukan dengan mengakses setiap elemen dari result matrix menggunakan pointer ``result_ptr`` dan menyalin nilainya ke setiap elemen dari shared memory yang terhubung dengan ``key1`` dan ``key2`` menggunakan ``shm_ptr1`` dan ``shm_ptr2``. Setelah penyalinan berhasil dilakukan, program akan menampilkan pesan sukses yang berisi key1 dan key2 yang telah digunakan untuk shared memory.

```c

    // wait for 10 seconds
    sleep(10);

    // detach shared memory
    if (shmdt(shm_ptr1) == -1) {
        perror("shmdt");
        exit(1);
    }
    // detach shared memory
    if (shmdt(shm_ptr2) == -1) {
        perror("shmdt");
        exit(1);
    }
    return 0;
}
```
Setelah hasil perkalian matriks di-copy ke shared memory dengan ``key1`` dan ``key2``, program akan menunggu selama 10 detik menggunakan fungsi ``sleep(10)``. Setelah itu, program akan melepaskan atau me-detach shared memory dengan menggunakan fungsi ``shmdt(shm_ptr1)`` dan ``shmdt(shm_ptr2)`` untuk masing-masing key. Setelah berhasil melepaskan shared memory, program akan mengembalikan nilai 0, menandakan program berakhir dengan sukses.


2. Membuat program c dengan nama ``cinta.c`` yang akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. ``cinta.c`` menerapkan konsep ``shared memory``. 
    
3. Setelah ditampilkan, lakukan pencarian nilai faktorial untuk setiap angka dari matriks. Hasilnya akan ditampilkan di layar dengan format seperti matriks. Faktorial dilakukan dengan konsep ``thread dan multithreading``

## Penjelasan ``cinta.c`` (Nomor 2 dan 3)

```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW 4
#define COL 5
#define SHM_SIZE 1024

struct args {
    int row;
    int col;
};
```
Penggunaan library yang digunakan pada ``cinta.c`` adalah sebagai berikut:

1. ``stdio.h``: mengandung fungsi-fungsi standar input/output seperti printf dan scanf

2. ``stdlib.h``: mengandung fungsi-fungsi standar C seperti malloc dan free

3. ``pthread.h``: mengandung fungsi-fungsi untuk thread-based programming pada sistem POSIX

4. ``sys/ipc.h``: mengandung definisi untuk fungsi-fungsi dalam message queue, shared memory, dan semaphore

5. ``sys/shm.h``: mengandung definisi untuk fungsi-fungsi dalam shared memory

6. ``time.h``: mengandung fungsi-fungsi untuk manipulasi waktu

Setelah library, define juga dilakukan sebagai berikut:

1. ``ROW``      : sebuah konstanta integer dengan nilai 4, merepresentasikan jumlah baris pada matriks yang akan digunakan dalam program

2. ``COL``      : sebuah konstanta integer dengan nilai 5, merepresentasikan jumlah kolom pada matriks yang akan digunakan dalam program

3. ``SHM_SIZE`` : sebuah konstanta integer dengan nilai 1024, merepresentasikan ukuran dari shared memory yang akan digunakan dalam program

Setelah Define, dilakukan struct yang didefinisikan bernama args. Struct ini memiliki dua variabel, yaitu:

1. ``row``: sebuah integer yang merepresentasikan nomor baris pada matriks

2. ``col``: sebuah integer yang merepresentasikan nomor kolom pada matriks

```c

unsigned long long int (*result)[5];
```
Deklarasi variabel global result yang merupakan pointer ke array 2 dimensi ``unsigned long long int``.

```c
void *factorial(void *arguments) {
    int row = ((struct args *) arguments)->row;
    int col = ((struct args *) arguments)->col;
    int num = result[row][col];
    unsigned long long int fac = 1;
    for (int i = 1; i <= num; i++) {
        fac *= i;
    }
    result[row][col] = fac;
    pthread_exit(NULL);
}
```
Fungsi ``factorial`` yang akan menghitung faktorial suatu angka, dengan menerima argumen berupa pointer ke struct ``args`` yang berisi koordinat elemen matriks result yang akan dihitung faktorialnya. Fungsi ini akan mengakses dan mengubah elemen matriks ``result`` sesuai dengan faktorial yang dihitung.
```c

void attach_shared_mem() {
    int shmid;
    key_t key = 1234;

    // attach shared memory segment
    if ((shmid = shmget(key, SHM_SIZE, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    if ((result = shmat(shmid, NULL, 0)) == (unsigned long long int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }
}
```
Fungsi ``attach_shared_mem`` yang akan meng-attach shared memory segment dengan menggunakan ``shmget`` dan ``shmat``. Fungsi ini juga akan mengecek apakah ``shmat`` berhasil atau tidak. Fungsi ini menggunakan key ``1234``.
```c

void print_matrix(unsigned long long int matrix[][COL], char* message) {
    printf("%s\n", message);
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%llu ", matrix[i][j]);
        }
        printf("\n");
    }
}
```
Fungsi ``print_matrix`` yang akan mencetak matriks ke dalam bentuk tabel menggunakan ``printf``. Fungsi ini menerima argumen berupa matriks dan sebuah string untuk ditampilkan sebagai pesan sebelum mencetak matriks.
```c

void calculate_factorial(pthread_t tid[][COL], struct args thread_args[][COL]) {
    // calculate factorial of each element using threads
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            thread_args[i][j].row = i;
            thread_args[i][j].col = j;
            pthread_create(&tid[i][j], NULL, factorial, (void *) &thread_args[i][j]);
        }
    }

    // wait for threads to finish
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            pthread_join(tid[i][j], NULL);
        }
    }
}
```
Fungsi ``calculate_factorial`` yang akan melakukan perhitungan faktorial dengan menggunakan thread. Fungsi ini akan memanggil fungsi ``pthread_create`` sebanyak ROW x COL kali, dengan mengirimkan argumen berupa koordinat matriks yang akan dihitung faktorialnya ke fungsi factorial. Setelah itu, fungsi ini akan memanggil fungsi ``pthread_join`` sebanyak ROW x COL kali untuk menunggu thread selesai melakukan perhitungan faktorial.
```c

void detach_shared_mem() {
    // detach shared memory segment
    shmdt(result);
}
```
Fungsi ``detach_shared_mem`` yang akan melakukan detach shared memory segment dengan menggunakan ``shmdt``.


```c

int main() {
    pthread_t tid[4][5];
    struct args thread_args[4][5];
    clock_t start, end;
    double cpu_time_used;
```
Membuat matriks 4 x 5 dari variabel ``tid`` yang bertipe ``pthread_t`` untuk menyimpan informasi thread. Lalu Membuat matriks 4 x 5 dari struktur ``args`` yang berisi informasi baris dan kolom. Deklarasi variabel ``start`` dan ``end`` yang bertipe ``clock_t`` untuk mengukur waktu eksekusi program. Setelah itu, deklarasi variabel ``cpu_time_used`` yang bertipe double untuk menyimpan waktu eksekusi program dalam detik.

```c

    // attach shared memory segment
    attach_shared_mem();
```
Memanggil fungsi attach_shared_mem() untuk meng-attach shared memory.
```c

    print_matrix(result, "Hasil Perkalian Matriks:");
```
Memanggil fungsi print_matrix() untuk mencetak matriks hasil perkalian ke layar.

```c

    // calculate factorial of each element using threads
    start = clock();
    calculate_factorial(tid, thread_args);
    end = clock();

    // calculate time taken
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
```
Menghitung waktu eksekusi program dengan mengurangi waktu akhir ``end`` dengan waktu awal ``start``, kemudian dibagi dengan CLOCKS_PER_SEC.
```c

    print_matrix(result, "Hasil Faktorial Matriks:");

    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", cpu_time_used);

    // detach shared memory segment
    detach_shared_mem();

    return 0;
}
```
Mengeluarkan hasil faktorial matriks dan waktu yang dibutuhkan. Setelah itu Memanggil fungsi ``detach_shared_mem()`` untuk meng-detach shared memory.


4. Membuat program C dengan nama ``sisop.c`` yang mirip dengan ``cinta.c`` tapi tidak menggunakan konsep ``thread dan multithreading``. Hal tersebut dilakukan agar bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan ``multithread`` dengan yang tidak.

## Penjelasan ``sisop.c`` (Nomor 4)
```c
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW 4
#define COL 5
#define SHM_SIZE 1024
```
Beberapa library dibutuhkan disini yaitu:
1. ``stdio.h``  : library untuk input-output (I/O) dalam bahasa C, seperti fungsi printf() dan scanf().

2. ``stdlib.h`` : library untuk fungsi umum dalam bahasa C, seperti fungsi malloc() dan free().

3. ``sys/ipc.h``: library untuk System V IPC (InterProcess Communication), digunakan untuk membuat kunci dan mengakses shared memory.

4. ``sys/shm.h``: library untuk mengakses shared memory, digunakan untuk membuat shared memory dan menghubungkan program ke shared memory.

5. ``time.h``   : library untuk waktu dan tanggal, digunakan untuk mengukur waktu eksekusi program.

Selain library, ada beberapa yang harus didefine juga yaitu:

1. ``ROW``      : jumlah baris pada matriks.

2. ``COL``      : jumlah kolom pada matriks.

3. ``SHM_SIZE`` : ukuran shared memory yang akan digunakan.

```c

unsigned long long int (*result)[COL];
```
Pointer pada array dua dimensi dengan ukuran kolom sebanyak ``COL``. Pointer ini akan digunakan untuk menunjuk pada shared memory yang sudah dibuat sebelumnya.

```c

void attach_shared_mem() {
    int shmid;
    key_t key = 5678;

    // attach shared memory segment
    if ((shmid = shmget(key, SHM_SIZE, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    if ((result = shmat(shmid, NULL, 0)) == (unsigned long long int (*)[COL]) -1) {
        perror("shmat");
        exit(1);
    }
}
```
Fungsi yang bertanggung jawab untuk meng-attach shared memory pada program yang sedang berjalan. Fungsi ini akan menggunakan key ``5678`` untuk mengambil shared memory segment yang sudah dibuat sebelumnya. Setelah berhasil di-attach, fungsi akan menyimpan alamat memori shared memory tersebut ke dalam variabel pointer result.
```c

void print_matrix(unsigned long long int matrix[][COL], const char* message) {
    printf("%s:\n", message);
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%llu ", matrix[i][j]);
        }
        printf("\n");
    }
}
```
Fungsi ini digunakan untuk mencetak matriks yang di-representasikan dalam bentuk array dua dimensi. Fungsi ini akan menerima dua parameter, yaitu matriks itu sendiri dan pesan yang akan dicetak sebelum mencetak matriks.
```c

void calculate_factorial() {
    // calculate factorial of each element
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            int num = result[i][j];
            unsigned long long int fac = 1;
            for (int k = 1; k <= num; k++) {
                fac *= k;
            }
            result[i][j] = fac;
        }
    }
}
```
fungsi ini digunakan untuk menghitung nilai faktorial dari setiap elemen pada matriks. Fungsi ini akan mengakses shared memory melalui variabel pointer ``result`` yang telah di-attach sebelumnya. Kemudian, fungsi akan menghitung faktorial dari setiap elemen dan menyimpannya kembali ke dalam shared memory.
```c

void detach_shared_mem() {
    // detach shared memory segment
    shmdt(result);
}
```
Fungsi ini bertanggung jawab untuk melepaskan shared memory yang sudah di-attach sebelumnya. Setelah fungsi ini dijalankan, variabel pointer ``result`` akan kehilangan akses pada shared memory.
```c

int main() {
    clock_t start, end;
    double cpu_time_used;
```
``start`` dan ``end`` bertipe data ``clock_t`` yang digunakan untuk menghitung waktu yang dibutuhkan oleh fungsi ``calculate_factorial()``. Lalu ``cpu_time_used`` bertipe data ``double`` yang digunakan untuk menyimpan waktu yang dibutuhkan untuk menjalankan fungsi ``calculate_factorial()`` dalam satuan detik.

```c

    // attach shared memory segment
    attach_shared_mem();
```
Memanggil fungsi attach_shared_mem() untuk meng-attach shared memory.
```c
    print_matrix(result, "Hasil Perkalian Matriks");
```
Memanggil fungsi print_matrix() untuk mencetak matriks hasil perkalian ke layar.
```c

    // calculate factorial of each element
    start = clock();
    calculate_factorial();
    end = clock();

    // calculate time taken
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
```
Menghitung waktu eksekusi program dengan mengurangi waktu akhir end dengan waktu awal start, kemudian dibagi dengan CLOCKS_PER_SEC.
```c

    print_matrix(result, "Hasil Faktorial Matriks");

    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", cpu_time_used);

    // detach shared memory segment
    detach_shared_mem();

    return 0;
}
```
Mengeluarkan hasil faktorial matriks dan waktu yang dibutuhkan. Setelah itu Memanggil fungsi detach_shared_mem() untuk meng-detach shared memory.
## Kesimpulan
Setelah dijalankan, berikut adalah hasil dari ``cinta.c``:
```c
Hasil Perkalian Matriks:
19 17 24 16 12
23 19 28 17 14
24 18 28 15 14
29 19 32 14 16
Hasil Faktorial Matriks:
121645100408832000 355687428096000 10611558092380307456 20922789888000 479001600
8128291617894825984 121645100408832000 12478583540742619136 355687428096000 87178291200
10611558092380307456 6402373705728000 12478583540742619136 1307674368000 87178291200
11390785281054474240 121645100408832000 12400865694432886784 87178291200 20922789888000

Waktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: 0.001789 detik
```
dan berikut adalah hasil dari ``sisop.c``:
```c
Hasil Perkalian Matriks:
19 17 24 16 12
23 19 28 17 14
24 18 28 15 14
29 19 32 14 16
Hasil Faktorial Matriks:
121645100408832000 355687428096000 10611558092380307456 20922789888000 479001600
8128291617894825984 121645100408832000 12478583540742619136 355687428096000 87178291200
10611558092380307456 6402373705728000 12478583540742619136 1307674368000 87178291200
11390785281054474240 121645100408832000 12400865694432886784 87178291200 20922789888000

Waktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: 0.000001 detik
```
Kesimpulan yang dapat diambil adalah program cinta.c mengalikan matriks dengan elemen hasilnya disimpan di shared memory, lalu menghitung faktorial dari setiap elemen matriks tersebut menggunakan thread. Sedangkan program sisop.c menghitung faktorial dari setiap elemen matriks yang sudah disimpan di shared memory.

Karena cinta.c menggunakan thread untuk menghitung faktorial, maka ada overhead tambahan untuk pembuatan dan penghancuran thread yang berdampak pada kinerja programnya. Sedangkan sisop.c langsung menghitung faktorial dari setiap elemen matriks tanpa overhead pembuatan dan penghancuran thread. Oleh karena itu, cinta.c lebih lambat daripada sisop.c.

## Note Tambahan
Pada program ``cinta.c`` dan ``sisop.c`` menggunakan key yang berbeda agar tidak overlapping dan mengeluarkan output yang tidak sesuai. ``cinta.c`` menggunakan ``key 1234`` sementara ``sisop.c`` menggunakan ``key 5678``


# Penjelasan Soal Nomor 3
Membuat sebuah layanan streaming dengan 2 script yaitu `stream.c` dan `user.c`. Script harus menerapakan
- Multiple sender dengan identifier menggunakan ``message queue``
- Melakukan perintah ``DECRYPT, ADD, PLAY.`` Jika perintah tidak ditemukan, maka keluarkan ``unknown commamd``
    - Untuk perintah ``DECRYPT`` akan menjalankan fungsi decrpytion pada lagu dan memasukannya ke playlist.txt serta ditampilkan di terminal stream
    - Terdapat 3 metode ``DECRYPT`` yaitu ``ROT13, Base64, dan HEX``
    - Untuk perintah ``ADD`` jika sudah ada lagunya maka, ``SONG ALREADY ON PLAYLIST``
    - Untuk perintah ``PLAY`` dapat memainkan lagu, memberi list lagu sesuai input, dan memberi pesan tidak ada lagu di playlist
- Wajib menggunakan ``semaphore`` untuk membatasi user, user maksimal 2. Jika lebih, maka user ketiga akan mendapatkan ``STREAM SYSTEM OVERLOAD``

## Penjelasan ``stream.c``
1. Berikut adalah library-library yang digunakan untuk menjalankan stream.c
```c
#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <regex.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <json-c/json.h> // sudo apt install libjson-c-dev
#include <semaphore.h>
```

2. Struct mesg_buffer nantinya akan menyimpan command perintah user, id user, dan tipe commandnya. Sebagaimana berikut,
```c
struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[1024];
} message;
```

3. Untuk fungsi ``matching_line()`` dibawah ini berfungsi untuk mencari berapa banyak jumlah lagu yang mengandung kata kunci dari variabel string ``str``. Proses pencarian dimulai dengan membuka file ``playlist.txt`` yang nantinya akan dibuat setelah user memanggil perintah **Decrypt**, kemudian file akan di traverse dan dicari ada berapa matching line dari sebuah string ``str`` melalui fungsi ``strcasestr()``.
```c
int matching_line(char *fname, char *str){ // return banyaknya baris pada file yang mengandung str
    FILE *fp;

    if((fp = fopen(fname, "r")) == NULL){ 
        return(-1);
    }

    int count = 0;
    char save[1024];
    while(fgets(save, sizeof(save), fp) != NULL){
        // set index akhir save menjadi akhir array \0 jika ditemukan newline
        if((strlen(save) > 0) && (save[strlen(save) - 1] == '\n')) save[strlen(save) - 1] = '\0'; 
        if((strcasestr(save, str)) != NULL) count++; // cari string str yang muncul di string save, ignore case sensitive.
    }

    if(fp){
        fclose(fp);
    }

    return count;
}
```

4. Untuk fungsi ``song_exist()`` dibawah ini kurang lebih hampir sama dengan fungsi ``matching_line()``, hanya saja fungsi ini dikhususkan ketika user memanggil perintah **ADD** untuk menambah lagu baru ke dalam playlist. 
```c
int song_exist(char *fname, char *str){  // cari lagu str apakah sudah ada di fname
    FILE *fp;
    
    if((fp = fopen(fname, "r")) == NULL){ 
        return(-1);
    }

    int count = 0;
    char save[1024];
    while(fgets(save, sizeof(save), fp) != NULL){ 
        if((strlen(save) > 0) && (save[strlen(save) - 1] == '\n')) save[strlen(save) - 1] = '\0';
        if((!strcasecmp(save, str))) count++; // jika ditemukan maka increment count
    }

    if(fp){
        fclose(fp);
    }

    return count;
}
```

5. Untuk fungsi ``decode_base64()`` dibawah ini berfungsi untuk melakukan decode dari lagu-lagu yang memiliki method dekripsi ber-tipe **base64**. Nantinya tiap index dari string akan di replace dengan hasil decoding yaitu variabel string ``res``.
```c
void decode_base64(char* str) { // proses decode judul lagu dengan format base64
    
    char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    char count = 0;
    int idx = 0;
    char buf[4];
    char* res = malloc(strlen(str) * 3 / 4 + 1);
    for(int i = 0; str[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && encoding_table[k] != str[i]; k++);
        buf[count++] = k;
        if(count == 4) {
            res[idx++] = (buf[0] << 2) + (buf[1] >> 4);
            if(buf[2] != 64) res[idx++] = (buf[1] << 4) + (buf[2] >> 2);
            if(buf[3] != 64) res[idx++] = (buf[2] << 6) + buf[3];
            count = 0;
        }
    }

    res[idx] = '\0';   
    strcpy(str, res); // copy result ke string awal.
}
```
6. Method decode dari lagu lainnya adalah **rot13**, nantinya tiap karakter pada string ``str`` akan dijumlahkan ataupun dikurangkan dengan value 13 dikarenakan hal tersebut merupakan bagian  dari proses decode untuk method **rot13**.
```c
void decode_rot13(char *str){ // proses decode judul lagu dengan format rot13
    char c;
    while(*str){
        c = *str;
        if(isalpha(c)){ 
            if(c >= 'a' && c <= 'm' || c >= 'A' && c <= 'M')*str += 13; // tambah 13 jika char adalah huruf antara a-m
            else *str -= 13; // kurang 13 jika char huruf antara n-z
        } str++;
    }
}
```

7. Method terakhir adalah metode decode **hex**,nantinya tiap karakter akan diubah / decode berdasarkan hexadecimal value-nya.
```c
void decode_hex(char *str){ // proses decode judul lagu dengan format hex
    char string[1024];
    int len = strlen(str);
    for(int i = 0, j = 0; j < len; ++i, j += 2){ 
        int val[1];
        sscanf(str + j, "%2x", val);
        string[i] = val[0];
        string[i + 1] = '\0';
    }
    strcpy(str, string);
}
```

8. Untuk fungsi ``sort()`` dibawah ini simpelnya hanya memanggil fungsi dari sistem linux yaitu ``sort`` dengan tujuan untuk mengurutkan file ``playlist.txt`` berdasarkan huruf abjad-nya.
```c
void sort(){
    char sort_comm[50];
    strcpy(sort_comm, "sort -u playlist.txt -o playlist.txt"); 
    system(sort_comm);
}
```

9. Kemudian terdapat fungsi ``decrypt()`` yang berfungsi ketika user memanggil perintah **DECRYPT** diawal. Nantinya, pada fungsi ini kita akan melakukan dekripsi terhadap file ``song-playlist.json`` yang telah diberikan melalui soal. Setiap object json pada file tersebut / judul lagu nya akan diperiksa method-nya berjenis apa melalui fungsi ``strstr``, semisal lagu tersebut memiliki method **rot13** maka nanti akan dipanggil fungsi ``decode_rot13()`` dengan parameter berupa ``judulLagu``-nya. Setelah di decrypt, kita akan melakukan proses ``write`` lagu tersebut ke dalam file ``playlist.txt`` melalui fungsi ``snprintf()`` dan outputkan playlist.txt nya pada terminal diakhiri dengan proses sorting file txt nya.
```c
void decrypt(){
    const char *fname = "song-playlist.json";
    struct json_object *parsed;
    parsed = json_object_from_file(fname); 

    if(!json_object_is_type(parsed, json_type_array)){ // cek tipe json -> array
        printf("Array not Found!\n");
        return;
    }

    int len = json_object_array_length(parsed);
    for(int i = 0; i < len; i++){

        struct json_object *obj = json_object_array_get_idx(parsed, i); // ambil setiap object json berdasarkan i
    
        if(!json_object_is_type(obj, json_type_object)){  // cek tipe json -> object
            printf("Item not Found!\n");
            continue;
        }

        struct json_object *method, *song;
        if(!json_object_object_get_ex(obj, "method", &method)){ // cari method nya berupa apa
            printf("Encryption Not Found!\n");
            continue;
        }
        if(!json_object_object_get_ex(obj, "song", &song)){ // cari judul lagu nya
            printf("Song Not Found!\n");
            continue;
        }

        char judulLagu[json_object_get_string_len(song) + 1]; // simpan object json pada arr of char
        strcpy(judulLagu, json_object_get_string(song));

        // panggil fungsi decode untuk masing-masing method
        if(strstr("rot13",json_object_get_string(method))) decode_rot13(judulLagu); 
        else if(strstr("hex", json_object_get_string(method))) decode_hex(judulLagu);
        else if(strstr("base64", json_object_get_string(method))) decode_base64(judulLagu); 
        
        char write_comm[strlen(judulLagu) + 30];
        snprintf(write_comm, sizeof(write_comm), "echo \"%s\" >> playlist.txt", judulLagu); 
        system(write_comm);
        printf("After Decrypt:\nMethod: %s\nSong: %s\n\n", json_object_get_string(method), judulLagu);
    }
    
    json_object_put(parsed); // decrement reference dari json sampai free
    sort();  // sort berdasarkan alfabet
}
```

10. Selanjutnya adalah fungsi ``addSong()``, fungsi yang akan dipanggil ketika user mengirim perintah **ADD** dilanjut dengan judul lagunya. Simpelnya, kia hanya perlu memeriksa apakah judul lagu yang ingin ditambah sudah tersedia atau belum di dalam file ``playlist.txt`` jika ternyata fungsi ``song_exist()`` mereturn value kurang dari 1 (belum ada), maka langsung panggil ``system()`` untuk echo judul lagu tersebut, lalu outputkan detail perintah bahwa user telah melakukan penambahan lagu. Selain itu, outputkan bahwa lagu sudah tersedia.
```c

void addSong(int uid, char *in){
    char txtname[] = "playlist.txt";
    char response[strlen(in)];
    strcpy(response, in);

    if(song_exist(txtname, response) < 1){ // cek jika lagu yang ingin ditambah sudah ada, <1 berarti belum ada
        char write_comm[strlen(response) + 30];
        snprintf(write_comm, sizeof(write_comm), "echo \"%s\" >> playlist.txt", response); 
        system(write_comm);
        sort();

        printf("USER \"%d\" ADD %s\n", uid, response);
    } else {
        printf("SONG ALREADY ON PLAYLIST\n");
    }
}
```

11. Kemudian ada fungsi ``play()`` yang akan dipanggil ketika user mengirim perintah **PLAY** sebuah judul lagu. Proses dimulai dengan mencari terlebih dahulu judul lagu yang dikirim, jika fungsi ``matching_line()`` mereturn value 0 maka outputkan bahwa tidak ada lagu yang mengandung kata kunci yang dikirim user. Jika return 1 atau hanya ada 1 lagu yang match permintaan user, maka ambil judul lagu tersebut dan copy ke variabel ``title`` dan outputkan bahwa user dengan id sekian sedang memutar lagu tersebut. Terakhir, jika ``matching_line()`` mereturn value lebih dari 1 maka outputkan list-list lagu apa saja yang mengandung kata kunci tersebut.
```c
void play(int uid, char *in){
    char txtname[] = "playlist.txt";
    char response[strlen(in) + 1];
    strcpy(response, in);

    int song_count = 0;
    song_count = matching_line(txtname, response); // hitung berapa lagu yang mengandung str response

    if(song_count == 0){
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", response);
    } 
    else if(song_count == 1){ // play lagu jika hanya ada 1 matching line
        FILE *fp;
        char save[1024];
        char title[1025];

        if((fp = fopen(txtname, "r")) == NULL){
            return;
        }

        while(fgets(save, sizeof(save), fp) != NULL){
            if((strlen(save) > 0) && (save[strlen(save) - 1] == '\n')) save[strlen(save) - 1] = '\0'; // 
            if((strcasestr(save, response)) != NULL) { // jika judul lagu response sesuai dengan yang ada di file.
                strcpy(title, save); // ambil dan simpan di title
            }
        }

        if(fp){
            fclose(fp);
        }
        
        printf("USER \"%d\" PLAYING %s\n", uid, title);
    } else { // play list lagu-lagu yang bersesuaian dengan response
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\"\n", song_count, response);
        FILE *fp;
        int count = 1;
        char save[1024];

        if((fp = fopen(txtname, "r")) == NULL){
            return;
        }

        while(fgets(save, sizeof(save), fp) != NULL){
            if(strcasestr(save, response) != NULL){ // jika kata kunci response ditemukan pada suatu lagu di save
                printf("%d. %s", count, save);
                count++;
            }
        }

        if(fp){
            fclose(fp);
        }
    }

}
```

12. Fungsi selanjutnya adalah ``listAllSongs()``, simpelnya fungsi ini akan melakukan command ``system()`` dengan tujuan untuk menampilkan isi dari file ``playlist.txt`` dengan command ``cat``.
```c

void listAllSongs(){
    sort();
    char cat_comm[50];
    strcpy(cat_comm, "cat playlist.txt");
    system(cat_comm);
    printf("\n");
}
```

13. Masuk ke fungsi ``main()``, pada awalnya kita akan men-declare variabel-variabel dari regular expression diikuti dengan pattern setiap command-nya, yaitu command **DECRYPT, LIST, PLAY, ADD**. Dilanjutkan dengan compile regex command dan juga patternya melalui fungsi ``regcomp()``. Setelah itu, kita akan melakukan setup / inisiasi dari semaphore **stream** dan **user** ke value-nya masing-masing yaitu 0 dan 2. Masuk ke perulangan ``while()``, disini program akan menunggu terlebih dahulu hingga user mengirim sebuah pesan / perintah. Nantinya, value dari variabel ``ret`` akan dibandingkan dengan perintah yang dikirim user, semisal user mengirim perintah **DECRYPT** maka nanti akan dipanggil fungsi ``decrypt()`` dan begitu pula untuk perintah **LIST, PLAY, dan ADD**, yang membedakan hanya pengiriman id user yang nantinya akan ditampilkan khusus untuk proses **PLAY** dan **ADD**.
```c
int main(){
    regex_t decrypt_rgx, list_rgx, play_rgx, add_rgx; // list regular expression command user
    
    // pattern regular expression untuk tiap command
    const char *decrypt_ptrn = "^DECRYPT\n$"; 
    const char *list_ptrn = "^LIST\n$"; 
    const char *play_ptrn = "^PLAY (.+)\n$"; 
    const char *add_ptrn = "^ADD (.+)\n$"; 
    
    int ret = regcomp(&decrypt_rgx, decrypt_ptrn, REG_EXTENDED);
    ret |= regcomp(&list_rgx, list_ptrn, REG_EXTENDED);
    ret |= regcomp(&play_rgx, play_ptrn, REG_EXTENDED);
    ret |= regcomp(&add_rgx, add_ptrn, REG_EXTENDED);
    
    key_t key;
    int msgid;
    key = ftok("stream_playlist", 50);
  
    sem_unlink("/user"); // unlink semaphore user
    sem_unlink("/stream"); // unlink semaphore stream

    sem_t *sem_stream = sem_open("/stream", O_CREAT | O_EXCL, 0660, 0); // inisialisasi sem_stream ke 0
    if(sem_stream == SEM_FAILED){
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }
    
    sem_t *sem_user = sem_open("/user", O_CREAT | O_EXCL, 0660, 2); // inisialisasi sem_user ke 2
    if(sem_user == SEM_FAILED){
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
    
    msgid = msgget(key, 0666 | IPC_CREAT);
    
    while(1){
        
        sem_wait(sem_stream); // tunggu user mengirim pesan

        msgrcv(msgid, &message, sizeof(message), 1, 0); // receive pesan yang dikirim user

        printf("USER \"%d\" SENT DATA %s\n", message.user_pid, message.mesg_text); // print detail pesan user

        regmatch_t match[2];
        
        ret = regexec(&decrypt_rgx, message.mesg_text, 0, NULL, 0); // set ret awal untuk decrypt

        if(ret == 0){  // jika user meminta decrypt
            decrypt(); // panggil fungsi decrypt
        } else { // jika bukan decrypt
            ret = regexec(&list_rgx, message.mesg_text, 0, NULL, 0); // set ret menjadi list
            if(ret == 0) { 
                listAllSongs(); // panggil fungsi listAllSongs
            } else { // Jika bukan list
                ret = regexec(&add_rgx, message.mesg_text, 2, match, 0); // set ret menjadi add
                if(ret == 0){
                    char addArg[match[1].rm_eo - match[1].rm_so + 1]; // ambil argumen add dari user
                    memcpy(addArg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                    addArg[match[1].rm_eo - match[1].rm_so] = '\0'; // set akhir string \0
                    addSong(message.user_pid, addArg);
                } else { // jika bukan add
                    ret = regexec(&play_rgx, message.mesg_text, 2, match, 0); // set ret menjadi play
                    if(ret == 0){
                        char playArg[match[1].rm_eo - match[1].rm_so + 1]; // ambil argumen play dari user
                        memcpy(playArg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                        playArg[match[1].rm_eo - match[1].rm_so] = '\0';
                        play(message.user_pid, playArg);
                    } else { // jika command tidak valid
                        printf("UNKNOWN COMMAND!\n");
                    }
                }
            }
        }
        
        sem_post(sem_user); // post signal semaphore ke user
    }
    return 0;
}

```
## Penjelasan ``user.c``
1. Berikut adalah library-library yang digunakan untuk menjalankan stream.c
```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>
```

2. Mendefinisikan sebuah struktur dengan nama ``mesg_buffer``. Struktur ini memiliki tiga anggota:
- ``mesg_type`` : variabel bilangan bulat panjang yang digunakan untuk menyimpan jenis pesan.
- ``user_pid``  : variabel bilangan bulat yang digunakan untuk menyimpan pengenal proses pengguna.
- ``mesg_text`` : sebuah array karakter dengan ukuran MAX, yang didefinisikan sebagai sebuah makro dengan nilai 1024. Anggota mesg_text digunakan untuk menyimpan teks pesan sebenarnya.
```c
#define MAX 1024

struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[MAX];
} message;
```
3. Fungsi ``create_message_queue`` menerima parameter pointer ke variabel ``msgid``. Fungsi ini digunakan untuk membuat sebuah antrian pesan menggunakan sistem V message queue. Pada awalnya, fungsi ini memanggil fungsi ``ftok`` untuk menghasilkan kunci yang digunakan untuk mengakses antrian pesan. Fungsi kemudian memanggil fungsi ``msgget`` untuk membuat atau mendapatkan antrian pesan yang terkait dengan kunci yang dihasilkan sebelumnya. Setelah berhasil membuat atau mendapatkan antrian pesan, fungsi mengatur anggota mesg_type dan user_pid dari message yang didefinisikan di luar fungsi.
```c
void create_message_queue(int *msgid) {
    key_t key = ftok("stream_playlist", 50);
    *msgid = msgget(key, 0666 | IPC_CREAT);
    message.mesg_type = 1;
    message.user_pid = getpid();
}
```
4. Fungsi ``open_semaphores`` menerima dua parameter yaitu pointer ke dua variabel bertipe ``sem_t``. Fungsi ini digunakan untuk membuka dua semaphore (sebuah objek sinkronisasi) yang sudah ada pada sistem dengan nama "/stream" dan "/user". Fungsi ini memanggil fungsi ``sem_open`` untuk membuka setiap semaphore. Jika pembukaan semaphore gagal, fungsi mencetak pesan kesalahan dan keluar dari program. Setelah berhasil membuka semaphore, alamat masing-masing semaphore disimpan ke variabel pointer yang dioperasikan.
```c

void open_semaphores(sem_t **sem_stream, sem_t **sem_user) {
    *sem_stream = sem_open("/stream", 0);
    if (*sem_stream == SEM_FAILED) {
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }

    *sem_user = sem_open("/user", 0);
    if (*sem_user == SEM_FAILED) {
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
}
```
5. Fungsi ``check_semaphore_value`` digunakan untuk mendapatkan nilai semaphore yang diberikan. Fungsi ini menerima parameter sebuah pointer ke sebuah variabel semaphore bertipe ``sem_t``. Fungsi memanggil fungsi ``sem_getvalue`` dengan argumen semaphore dan alamat variabel ``sem_val`` sebagai argumen kedua. ``sem_getvalue`` mengisi variabel ``sem_val`` dengan nilai saat ini dari semaphore yang diberikan. Fungsi kemudian mengembalikan nilai variabel ``sem_val`` sebagai hasil. Dengan demikian, fungsi ``check_semaphore_value`` memberikan cara untuk memeriksa nilai semaphore saat ini dan digunakan di kode lain untuk mengambil tindakan yang sesuai berdasarkan nilai semaphore tersebut.
```c
int check_semaphore_value(sem_t *sem) {
    int sem_val;
    sem_getvalue(sem, &sem_val);

    return sem_val;
}
```
6. Fungsi ``wait_semaphore`` digunakan untuk menunggu atau mengambil (menurunkan) nilai semaphore yang diberikan. Fungsi ini menerima parameter sebuah pointer ke sebuah variabel semaphore bertipe ``sem_t``. Fungsi memanggil fungsi ``sem_wait`` dengan argumen semaphore yang diberikan. ``sem_wait`` akan menunggu sampai nilai semaphore tersebut dapat diturunkan (nilai menjadi lebih rendah dari 0) dan kemudian akan mengambil nilai semaphore tersebut. Fungsi ``wait_semaphore`` akan terus menunggu sampai semaphore tersebut dapat diturunkan dan kemudian akan kembali dari panggilan sem_wait saat nilai semaphore berhasil diambil. Dengan demikian, fungsi ``wait_semaphore`` digunakan untuk sinkronisasi antara beberapa thread atau proses dalam program.
```c

void wait_semaphore(sem_t *sem) {
    sem_wait(sem);
}
```
7. Fungsi ``post_semaphore`` digunakan untuk meningkatkan nilai semaphore yang diberikan. Fungsi ini menerima parameter sebuah pointer ke sebuah variabel semaphore bertipe ``sem_t``. Fungsi memanggil fungsi ``sem_post`` dengan argumen semaphore yang diberikan. ``sem_post`` akan meningkatkan nilai semaphore tersebut dan membangunkan thread atau proses yang mungkin menunggu pada semaphore tersebut. Dengan demikian, fungsi ``post_semaphore`` digunakan untuk memberi sinyal kepada thread atau proses lain bahwa mereka dapat melanjutkan eksekusi setelah menunggu semaphore yang diberikan. Fungsi ini digunakan bersama dengan ``wait_semaphore`` untuk sinkronisasi antara thread atau proses dalam program.

```c
void post_semaphore(sem_t *sem) {
    sem_post(sem);
}
```
8. Fungsi ``send_message`` digunakan untuk mengirim pesan melalui message queue pada sistem operasi. Fungsi ini menerima parameter sebuah integer ``msgid`` yang merepresentasikan message queue identifier, yaitu sebuah nomor unik yang digunakan untuk mengidentifikasi message queue pada sistem. Fungsi ``msgsnd`` adalah fungsi sistem yang digunakan untuk mengirim pesan ke message queue. Fungsi ini menerima beberapa argumen, yaitu message queue identifier, pointer ke struct message yang akan dikirim, ukuran dari message tersebut dalam byte, dan flag yang menentukan perilaku fungsi ketika message queue sudah penuh. Pada kode di atas, fungsi ``send_message`` memanggil ``msgsnd`` dengan argumen msgid, yaitu message queue identifier, ``&message``, yaitu pointer ke struct message yang akan dikirim, ``sizeof(message)``, yaitu ukuran dari struct message dalam byte, dan 0, yaitu flag default yang menunjukkan bahwa fungsi akan block jika message queue sudah penuh. Dengan demikian, fungsi ``send_message`` digunakan untuk mengirim pesan melalui message queue dan memungkinkan komunikasi antara proses atau thread yang berbeda pada sistem operasi.
```c
void send_message(int msgid) {
    msgsnd(msgid, &message, sizeof(message), 0);
}
```
9. Fungsi ``read_input`` bertujuan untuk membaca input dari pengguna melalui standard input (stdin) dan menyimpan input tersebut pada variabel ``message.mesg_text``. Fungsi ``printf`` digunakan untuk mencetak pesan ke layar, pada kode di atas pesan yang dicetak adalah "Insert Data:". Fungsi fgets digunakan untuk membaca input dari pengguna melalui standard input (stdin) dan menyimpan input tersebut pada variabel ``message.mesg_text``. Fungsi ini menerima beberapa argumen, yaitu pointer ke variabel buffer yang akan digunakan untuk menyimpan input, ukuran dari buffer tersebut, dan pointer ke file stream tempat input akan dibaca. Pada kode di atas, ukuran buffer yang digunakan adalah ``MAX`` yang didefinisikan sebelumnya dengan nilai 1024. Dengan demikian, fungsi ``read_input`` digunakan untuk membaca input dari pengguna dan menyimpannya pada variabel ``message.mesg_text``, yang kemudian dapat digunakan untuk mengirim pesan melalui message queue pada sistem operasi.
```c
void read_input() {
    printf("Insert Data:");
    fgets(message.mesg_text, MAX, stdin);
}
```
10. Fungsi ``main`` adalah fungsi utama yang dieksekusi ketika program dijalankan. Pada kode di atas, fungsi main memiliki beberapa langkah kerja yang dijelaskan sebagai berikut:

    1. **Membuat message queue dan membuka semaphore:** fungsi ``create_message_queue`` digunakan untuk membuat message queue dan menginisialisasi variabel ``msgid`` dengan message queue identifier, sedangkan fungsi ``open_semaphores`` digunakan untuk membuka semaphore ``sem_stream`` dan ``sem_user``.
    2. **Memeriksa semaphore: dengan memanggil fungsi** ``check_semaphore_value``, program memeriksa apakah semaphore ``sem_user`` memiliki nilai 0 atau tidak. Jika nilai semaphore ``sem_user`` adalah 0, maka program mencetak pesan ``STREAM SYSTEM OVERLOAD`` dan keluar dari program dengan nilai 1.
    3. **Menunggu input: dengan memanggil fungsi** ``wait_semaphore(sem_user)``, program menunggu sampai semaphore ``sem_user`` tersedia. Kemudian program membaca input dari pengguna dengan memanggil fungsi ``read_input``.
    4. **Keluar dari program:** jika input yang dibaca adalah "EXIT\n", program akan keluar dari loop ``while`` dan mengembalikan nilai 0.
    5. **Mengirim pesan:** jika input yang dibaca bukan "EXIT\n", program akan mengirim pesan ke message queue dengan memanggil fungsi ``send_message`` dan mencetak pesan "Data sent: [isi pesan]" di layar. Kemudian program membebaskan semaphore ``sem_stream`` dengan memanggil fungsi ``post_semaphore(sem_stream)``.
    6. **Kembali ke langkah 3:** program kembali ke langkah 3 dan menunggu input dari pengguna.

    Dengan demikian, fungsi ``main`` pada kode di atas membentuk sebuah loop yang terus menunggu input dari pengguna, mengirim pesan ke message queue, dan menunggu semaphore tersedia sebelum membaca input berikutnya. Program akan berjalan terus menerus hingga pengguna memasukkan input "EXIT\n".
```c
int main() {
    int msgid;
    sem_t *sem_stream, *sem_user;

    create_message_queue(&msgid);
    open_semaphores(&sem_stream, &sem_user);

    if (check_semaphore_value(sem_user) == 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
        return 1;
    }

    while(1) {
        printf("Waiting...\n");
        wait_semaphore(sem_user);
        read_input();

        if(strcmp("EXIT\n", message.mesg_text) == 0) {
            post_semaphore(sem_user);
            return 0;
        }

        send_message(msgid);
        printf("Data sent: %s\n", message.mesg_text);

        post_semaphore(sem_stream);
    }

    return 0;
}
```

# Penjelasan Soal Nomor 4

## Penjelasan Soal Nomor 4A (unzip)

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

void download()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan download\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char link[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        char *argv[] = {"wget", "-q", link, "-O", "hehe.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

void unzip()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan unzip pada hehe.zip\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"unzip", "-q", "hehe.zip", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses unzip pada hehe.zip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}

int main()
{
    download();
    unzip();
    return 0;
}
```

### Fungsi Void Download
```c
void download()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan download\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char link[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
        char *argv[] = {"wget", "-q", link, "-O", "hehe.zip", NULL};
        execv("/usr/bin/wget", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses download tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}
```
Fungsi ini bertanggung jawab untuk melakukan download file dari internet menggunakan perintah wget. Pertama, fungsi ini menggunakan fork() untuk membuat proses anak yang akan mengeksekusi perintah execv() dengan argumen yang sesuai untuk melakukan download file. Proses anak akan mengeksekusi perintah wget dengan argumen -q untuk mendownload file secara diam-diam dan argumen -O untuk memberi nama file hasil unduhan, dalam hal ini hehe.zip. Jika proses anak berhasil mengeksekusi perintah execv(), maka proses anak akan keluar dengan menggunakan exit(0). Setelah proses download selesai, program utama akan menunggu proses anak selesai dengan menggunakan wait().

### Fungsi Unzip 
```c
void unzip()
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal melakukan unzip pada hehe.zip\n");
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"unzip", "-q", "hehe.zip", NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses unzip pada hehe.zip tidak berhenti dengan normal\n");
        exit(EXIT_FAILURE);
    }
}
```
Fungsi ini bertanggung jawab untuk mengekstrak file zip yang telah didownload menggunakan perintah unzip. Seperti fungsi download(), fungsi ini juga menggunakan fork() untuk membuat proses anak yang akan mengeksekusi perintah execv() dengan argumen yang sesuai untuk melakukan ekstraksi file. Proses anak akan mengeksekusi perintah unzip dengan argumen -q untuk mengekstrak file secara diam-diam. Jika proses anak berhasil mengeksekusi perintah execv(), maka proses anak akan keluar dengan menggunakan exit(0). Setelah proses ekstraksi selesai, program utama akan menunggu proses anak selesai dengan menggunakan wait().

### Fungsi Main
```c
int main()
{
    download();
    unzip();
    return 0;
}
```
Fungsi utama ini akan memanggil fungsi download() dan unzip() untuk melakukan download dan ekstraksi file. Setelah kedua fungsi selesai dieksekusi, program utama akan keluar dengan status 0 jika semua proses berjalan dengan normal atau EXIT_FAILURE jika ada proses yang gagal.

Secara keseluruhan, program ini menggunakan konsep multi-processing untuk menjalankan perintah-perintah pada command-line interface. Dengan cara ini, program dapat menjalankan perintah-perintah tersebut secara terpisah dan tidak akan mengganggu proses utama.

## Penjelasan Soal Nomor 4B (categorize)
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <errno.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

typedef struct Extension
{
    char name[10];
    int ct_file;
}
Ext;

Ext extList[100];
int ct_ext;
int ct_other;
int max_folder_size;

char *get_timestamp()
{
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char *timestamp = (char *) malloc(20 * sizeof(char));

    strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", tm_info);
    return timestamp;
}

void write_log(char *log)
{
    FILE *fp;
    fp = fopen("log.txt", "a");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    fprintf(fp, "%s %s\n", get_timestamp(), log);
    fclose(fp);
}

void make_dir(char *dirname)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat folder %s\n", dirname);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan folder %s tidak berhenti dengan normal\n", dirname);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MADE %s", dirname + 2);
    write_log(log);
}

void make_file(char *filename)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"touch", filename, NULL};
        execv("/usr/bin/touch", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
}

void move_file(char *filename, char *dest, char *ext)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal memindahkan file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mv", filename, dest, NULL};
        execv("/usr/bin/mv", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pemindahan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MOVED %s file : %s > %s", ext, filename, dest + 2);
    write_log(log);
}

bool check_extension(char *filename, char *ext)
{
    int len_file = strlen(filename);
    int len_ext = strlen(ext);

    if(len_file <= len_ext){
        return false;
    }
    char temp[len_ext + 1];
    strcpy(temp, filename + len_file - len_ext);

    for(int i = 0; i < len_ext; ++i){
        if(isalpha(temp[i]))
            temp[i] = tolower(temp[i]);
    }
    return (strcmp(temp, ext) == 0);
}

void *categorizing(void *args)
{
    char *path = (char *) args;
    DIR *dir = opendir(path);
    struct dirent *ent;

    if(dir == NULL){
        printf("Gagal membuka direktori %s\n", path);
        pthread_exit(NULL);
    }
    while((ent = readdir(dir)) != NULL)
    {
        if(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){
            continue;
        }
        if(ent->d_type == DT_DIR)
        {
            char sub_dir[1000], log[2000];
            sprintf(sub_dir, "%s/%s", path, ent->d_name);

            sprintf(log, "ACCESSED %s", sub_dir);
            write_log(log);

            pthread_t thread;
            pthread_create(&thread, NULL, categorizing, (void *) sub_dir);
            pthread_join(thread, NULL);
        }
        else if(ent->d_type == DT_REG)
        {
            bool other = true;
            char file_path[1000];
            sprintf(file_path, "%s/%s", path, ent->d_name);

            for(int i = 0; i < ct_ext; ++i)
            {
                if(check_extension(ent->d_name, extList[i].name))
                {
                    char dest[1000];
                    extList[i].ct_file++;
                    int idx_folder = floor(extList[i].ct_file / max_folder_size);

                    if(idx_folder == 0)
                        sprintf(dest, "./categorized/%s", extList[i].name);
                    else {
                        sprintf(dest, "./categorized/%s (%d)", extList[i].name, idx_folder + 1);
                        if(access(dest, F_OK) != 0)
                            make_dir(dest);
                    }
                    move_file(file_path, dest, extList[i].name);
                    other = false;
                    break;
                }
            }
            if(other){
                move_file(file_path, "./categorized/other", "other");
                ct_other++;
            }
        }
    }
    closedir(dir);
    pthread_exit(NULL);
}

void read_files()
{
    char buffer[10];
    FILE *fp;
    fp = fopen("extensions.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file extensions.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
    {
        if(buffer[strlen(buffer) - 1] == '\n')
            buffer[strlen(buffer) - 1] = '\0';
        strcpy(extList[ct_ext].name, buffer);
        ct_ext++;
    }
    for(int i = 0; i < ct_ext; ++i){
        extList[i].name[strlen(extList[i].name) - 1] = '\0';
    }
    fp = fopen("max.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file max.txt\n");
        exit(1);
    }
    while(fgets(buffer, 10, fp) != NULL)
        max_folder_size = atoi(buffer);
    fclose(fp);
}

void make_folders()
{
    make_dir("./categorized");
    for(int i = 0; i < ct_ext; ++i){
        char path[1000];
        sprintf(path, "./categorized/%s", extList[i].name);
        make_dir(path);
    }
    make_dir("./categorized/other");
}

void swap_Ext(Ext *a, Ext *b)
{
    Ext temp = *a;
    *a = *b;
    *b = temp;
}

void sort_Ext()
{
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}

void print_Ext()
{
    for(int i = 0; i < ct_ext; ++i)
        printf("extension_%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("%-15s : %d\n", "other", ct_other);
}

int main()
{
    read_files();
    make_folders();
    make_file("log.txt");

    char path[1000] = "files";
    pthread_t thread;
    pthread_create(&thread, NULL, categorizing, (void *) path);
    pthread_join(thread, NULL);

    sort_Ext();
    print_Ext();
    return 0;
}

```

program yang mengurutkan file berdasarkan ekstensi mereka. Program akan menerima jalur direktori sebagai inputnya, dan akan mencari semua file di direktori dan subdirektori. Jika file memiliki ekstensi yang dikenali, maka file tersebut akan dipindahkan ke folder dengan nama yang sama dengan ekstensinya. Jika file tidak memiliki ekstensi yang dikenali, maka file tersebut akan dipindahkan ke folder "other".

Program menggunakan beberapa thread untuk menelusuri direktori dan subdirektorinya secara bersamaan. Program ini juga menulis file log yang berisi informasi tentang aktivitas program, termasuk pembuatan folder, pembuatan file, dan pemindahan file.

Program ini dimulai dengan mendefinisikan struktur Ekstensi yang berisi nama ekstensi dan jumlah file dengan ekstensi tersebut. Program ini juga menginisialisasi sebuah array dari struktur Ekstensi dengan maksimum 100 elemen.


```c
void make_dir(char *dirname)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat folder %s\n", dirname);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mkdir", dirname, NULL};
        execv("/usr/bin/mkdir", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan folder %s tidak berhenti dengan normal\n", dirname);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MADE %s", dirname + 2);
    write_log(log);
}

void make_file(char *filename)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal membuat file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"touch", filename, NULL};
        execv("/usr/bin/touch", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pembuatan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
}

void move_file(char *filename, char *dest, char *ext)
{
    pid_t child_id;
    child_id = fork();

    if(child_id < 0){
        printf("Gagal memindahkan file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        char *argv[] = {"mv", filename, dest, NULL};
        execv("/usr/bin/mv", argv);
        exit(0);
    }
    int status;
    wait(&status);

    if(!WIFEXITED(status)){
        printf("Proses pemindahan file %s tidak berhenti dengan normal\n", filename);
        exit(EXIT_FAILURE);
    }
    char log[2000];
    sprintf(log, "MOVED %s file : %s > %s", ext, filename, dest + 2);
    write_log(log);
}
```
Program ini mendefinisikan beberapa fungsi untuk membuat direktori, file, dan memindahkan file. Fungsi make_dir membuat direktori menggunakan panggilan sistem mkdir. Fungsi make_file membuat file menggunakan panggilan sistem touch. Fungsi move_file memindahkan file menggunakan panggilan sistem mv.


```c
void *categorizing(void *args)
{
    char *path = (char *) args;
    DIR *dir = opendir(path);
    struct dirent *ent;

    if(dir == NULL){
        printf("Gagal membuka direktori %s\n", path);
        pthread_exit(NULL);
    }
    while((ent = readdir(dir)) != NULL)
    {
        if(strcmp(ent->d_name, ".") == 0 || strcmp(ent->d_name, "..") == 0){
            continue;
        }
        if(ent->d_type == DT_DIR)
        {
            char sub_dir[1000], log[2000];
            sprintf(sub_dir, "%s/%s", path, ent->d_name);

            sprintf(log, "ACCESSED %s", sub_dir);
            write_log(log);

            pthread_t thread;
            pthread_create(&thread, NULL, categorizing, (void *) sub_dir);
            pthread_join(thread, NULL);
        }
        else if(ent->d_type == DT_REG)
        {
            bool other = true;
            char file_path[1000];
            sprintf(file_path, "%s/%s", path, ent->d_name);

            for(int i = 0; i < ct_ext; ++i)
            {
                if(check_extension(ent->d_name, extList[i].name))
                {
                    char dest[1000];
                    extList[i].ct_file++;
                    int idx_folder = floor(extList[i].ct_file / max_folder_size);

                    if(idx_folder == 0)
                        sprintf(dest, "./categorized/%s", extList[i].name);
                    else {
                        sprintf(dest, "./categorized/%s (%d)", extList[i].name, idx_folder + 1);
                        if(access(dest, F_OK) != 0)
                            make_dir(dest);
                    }
                    move_file(file_path, dest, extList[i].name);
                    other = false;
                    break;
                }
            }
            if(other){
                move_file(file_path, "./categorized/other", "other");
                ct_other++;
            }
        }
    }
    closedir(dir);
    pthread_exit(NULL);
}
```
Program menggunakan fungsi kategorisasi yang dijalankan oleh beberapa thread untuk menelusuri direktori dan subdirektorinya. Fungsi kategorisasi memeriksa apakah setiap file memiliki ekstensi yang dikenali dan memindahkannya ke folder yang tepat. Jika file tidak memiliki ekstensi yang dikenali, maka file tersebut dipindahkan ke folder "other".

```c
bool check_extension(char *filename, char *ext)
{
    int len_file = strlen(filename);
    int len_ext = strlen(ext);

    if(len_file <= len_ext){
        return false;
    }
    char temp[len_ext + 1];
    strcpy(temp, filename + len_file - len_ext);

    for(int i = 0; i < len_ext; ++i){
        if(isalpha(temp[i]))
            temp[i] = tolower(temp[i]);
    }
    return (strcmp(temp, ext) == 0);
}
```
Program menggunakan fungsi check_extension untuk memeriksa apakah suatu file memiliki ekstensi yang dikenali. Fungsi ini mengambil dua argumen: nama file dan ekstensi. Fungsi ini mengembalikan nilai true jika nama file memiliki ekstensi tersebut dan false jika sebaliknya.


```c
char *get_timestamp()
{
    time_t now = time(NULL);
    struct tm *tm_info = localtime(&now);
    char *timestamp = (char *) malloc(20 * sizeof(char));

    strftime(timestamp, 20, "%d-%m-%Y %H:%M:%S", tm_info);
    return timestamp;
}
```
Program menggunakan fungsi get_timestamp untuk mendapatkan timestamp saat ini dalam format "dd-mm-yyyy HH:MM:SS". Fungsi ini mengembalikan string yang berisi timestamp.

```c
void write_log(char *log)
{
    FILE *fp;
    fp = fopen("log.txt", "a");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    fprintf(fp, "%s %s\n", get_timestamp(), log);
    fclose(fp);
}
```
Program menggunakan fungsi write_log untuk menulis informasi tentang aktivitas program ke file log. Fungsi ini mengambil string sebagai argumennya dan menambahkannya ke file log dengan timestamp saat ini.

```c
int main()
{
    read_files();
    make_folders();
    make_file("log.txt");

    char path[1000] = "files";
    pthread_t thread;
    pthread_create(&thread, NULL, categorizing, (void *) path);
    pthread_join(thread, NULL);

    sort_Ext();
    print_Ext();
    return 0;
}
```
Fungsi utama menginisialisasi variabel ct_ext menjadi 0 dan variabel ct_other menjadi 0. Kemudian membaca ukuran maksimum folder dari pengguna dan menginisialisasi variabel max_folder_size dengan nilai tersebut. Program kemudian membuat folder "categorized" dan folder "other" menggunakan fungsi make_dir.

Fungsi utama kemudian menggunakan fungsi kategorisasi untuk menelusuri direktori dan subdirektorinya secara bersamaan. Akhirnya, program menulis pesan log yang menunjukkan bahwa program telah selesai dijalankan.

### Penjelasan Soal Nomor 4C (logchecker)
```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <unistd.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>

int ct_acc;
int ct_fd;
int ct_ext;

typedef struct Folder
{
    char name[1000];
    int ct_file;
}
FD;

typedef struct Extension
{
    char name[10];
    int ct_file;
}
Ext;

void swap_FD(FD *a, FD *b)
{
    FD temp = *a;
    *a = *b;
    *b = temp;
}

void swap_Ext(Ext *a, Ext *b)
{
    Ext temp = *a;
    *a = *b;
    *b = temp;
}

void sortAll(FD *fdList, Ext *extList)
{
    for(int j = ct_fd - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(fdList[i].ct_file > fdList[i + 1].ct_file)
                swap_FD(fdList + i, fdList + i + 1);
        }
    }
    for(int j = ct_ext - 1; j > 0; --j){
        for(int i = 0; i < j; ++i){
            if(extList[i].ct_file > extList[i + 1].ct_file)
                swap_Ext(extList + i, extList + i + 1);
        }
    }
}

void printAll(FD *fdList, Ext *extList)
{
    printf("TOTAL ACCESSED : %d\n\n", ct_acc);

    printf("--- LIST FOLDER ---\n");
    for(int i = 0; i < ct_fd; ++i)
        printf("%-19s : %d\n", fdList[i].name, fdList[i].ct_file);
    printf("\n");

    printf("--- LIST EXTENSION ---\n");
    for(int i = 0; i < ct_ext; ++i)
        printf("%-5s : %d\n", extList[i].name, extList[i].ct_file);
    printf("\n");
}

void read_log(FD *fdList, Ext *extList)
{
    char buffer[2000];
    FILE *fp;
    fp = fopen("log.txt", "r");

    if(fp == NULL){
        printf("Gagal membuka file log.txt\n");
        exit(1);
    }
    while(fgets(buffer, 1000, fp) != NULL)
    {
        if(strstr(buffer, " ACCESSED ") != NULL)
            ct_acc++;
        else if(strstr(buffer, " MADE ") != NULL)
        {
            char fd[1000];
            sscanf(buffer, "%*s %*s %*s %[^\n]s", fd);
            fd[strlen(fd)] = '\0';

            int i;
            for(i = 0; i < ct_fd; ++i){
                if(strcmp(fd, fdList[i].name) == 0)
                    break;
            }
            if(i == ct_fd){
                strcpy(fdList[i].name, fd);
                fdList[i].ct_file = 0;
                ct_fd++;
            }
        }
        else if(strstr(buffer, " MOVED ") != NULL)
        {
            char ext[10], fd[1000];
            sscanf(buffer, "%*s %*s %*s %s", ext);

            char *temp = strstr(buffer, " > ");
            strcpy(fd, temp + 3);
            fd[strlen(fd) - 1] = '\0';

            int i;
            for(i = 0; i < ct_ext; ++i){
                if(strcmp(ext, extList[i].name) == 0){
                    extList[i].ct_file++;
                    break;
                }
            }
            if(i == ct_ext){
                strcpy(extList[i].name, ext);
                extList[i].ct_file = 1;
                ct_ext++;
            }
            for(i = 0; i < ct_fd; ++i){
                if(strcmp(fd, fdList[i].name) == 0){
                    fdList[i].ct_file++;
                    break;
                }
            }
        }
    }
    fclose(fp);
}

int main()
{
    FD fdList[400];
    Ext extList[100];
    read_log(fdList, extList);
    sortAll(fdList, extList);
    printAll(fdList, extList);
    return 0;
}
```

Program yang diberikan adalah program dalam bahasa C yang bertujuan untuk menganalisis sebuah file log dengan format tertentu, kemudian menampilkan informasi terkait jumlah file yang diakses dan jumlah file per folder dan ekstensi.


Pada awal program, dilakukan deklarasi beberapa variabel global yang digunakan dalam program yaitu ct_acc (jumlah total file yang diakses), ct_fd (jumlah total folder), dan ct_ext (jumlah total ekstensi).

Selanjutnya, didefinisikan dua buah struktur yaitu FD dan Ext. Struktur FD memiliki dua elemen yaitu name (nama folder) dan ct_file (jumlah file pada folder tersebut), sedangkan struktur Ext memiliki dua elemen yaitu name (nama ekstensi) dan ct_file (jumlah file dengan ekstensi tersebut).

Kemudian, didefinisikan beberapa fungsi yaitu swap_FD, swap_Ext, sortAll, printAll, dan read_log.

Fungsi swap_FD dan swap_Ext digunakan untuk menukar elemen dua buah struktur FD dan Ext pada saat pengurutan. Fungsi sortAll digunakan untuk mengurutkan struktur FD dan Ext secara ascending berdasarkan jumlah file yang ada pada masing-masing folder dan ekstensi. Sedangkan fungsi printAll digunakan untuk menampilkan informasi jumlah file yang diakses, jumlah file per folder, dan jumlah file per ekstensi.

Fungsi read_log adalah fungsi utama yang digunakan untuk membaca isi dari file log dan melakukan pengolahan data. Pada fungsi ini, dibuka file log.txt yang berisi data-data mengenai akses dan aktivitas file. Selanjutnya, dilakukan pembacaan file secara baris per baris menggunakan fungsi fgets dan dilakukan pemeriksaan terhadap kata kunci dalam setiap baris menggunakan fungsi strstr. Jika pada baris terdapat kata kunci "ACCESSED", maka variabel ct_acc akan ditambah 1. Jika terdapat kata kunci "MADE", maka dilakukan pembacaan nama folder yang dibuat, dan dilakukan pengecekan apakah folder tersebut sudah ada pada struktur FD. Jika belum, maka folder baru tersebut akan dimasukkan ke dalam struktur FD. Jika terdapat kata kunci "MOVED", maka dilakukan pembacaan nama ekstensi dan folder tujuan dari file yang dipindahkan, dan dilakukan pengecekan apakah ekstensi tersebut sudah ada pada struktur Ext dan folder tujuan sudah ada pada struktur FD. Jika belum, maka ekstensi baru dan folder baru tersebut akan dimasukkan ke dalam struktur Ext dan FD masing-masing.

Setelah selesai membaca file log dan melakukan pengolahan data, program akan memanggil fungsi sortAll untuk mengurutkan struktur FD dan Ext. Kemudian, hasil pengolahan data akan ditampilkan ke layar menggunakan fungsi printAll.
