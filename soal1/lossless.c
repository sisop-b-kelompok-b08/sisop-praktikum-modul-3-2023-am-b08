#include<stdio.h> 
#include<stdlib.h> 
#include<unistd.h> 
#include<sys/types.h> 
#include<string.h> 
#include<sys/wait.h> 
#include<ctype.h>
#include <sys/ipc.h>
#include <sys/shm.h>

// This constant can be avoided by explicitly
// calculating height of Huffman Tree
#define MAX_TREE_HT 100
 
// A Huffman tree node
struct MinHeapNode {
 
    // One of the input characters
    char data;
 
    // Frequency of the character
    unsigned freq;
 
    // Left and right child of this node
    struct MinHeapNode *left, *right;
};
 
// A Min Heap:  Collection of
// min-heap (or Huffman tree) nodes
struct MinHeap {
 
    // Current size of min heap
    unsigned size;
 
    // capacity of min heap
    unsigned capacity;
 
    // Array of minheap node pointers
    struct MinHeapNode** array;
};
 
// A utility function allocate a new
// min heap node with given character
// and frequency of the character
struct MinHeapNode* newNode(char data, unsigned freq)
{
    struct MinHeapNode* temp = (struct MinHeapNode*)malloc(
        sizeof(struct MinHeapNode));
 
    temp->left = temp->right = NULL;
    temp->data = data;
    temp->freq = freq;
 
    return temp;
}
 
// A utility function to create
// a min heap of given capacity
struct MinHeap* createMinHeap(unsigned capacity)
{
 
    struct MinHeap* minHeap
        = (struct MinHeap*)malloc(sizeof(struct MinHeap));
 
    // current size is 0
    minHeap->size = 0;
 
    minHeap->capacity = capacity;
 
    minHeap->array = (struct MinHeapNode**)malloc(
        minHeap->capacity * sizeof(struct MinHeapNode*));
    return minHeap;
}
 
// A utility function to
// swap two min heap nodes
void swapMinHeapNode(struct MinHeapNode** a,
                     struct MinHeapNode** b)
{
 
    struct MinHeapNode* t = *a;
    *a = *b;
    *b = t;
}
 
// The standard minHeapify function.
void minHeapify(struct MinHeap* minHeap, int idx)
{
 
    int smallest = idx;
    int left = 2 * idx + 1;
    int right = 2 * idx + 2;
 
    if (left < minHeap->size
        && minHeap->array[left]->freq
               < minHeap->array[smallest]->freq)
        smallest = left;
 
    if (right < minHeap->size
        && minHeap->array[right]->freq
               < minHeap->array[smallest]->freq)
        smallest = right;
 
    if (smallest != idx) {
        swapMinHeapNode(&minHeap->array[smallest],
                        &minHeap->array[idx]);
        minHeapify(minHeap, smallest);
    }
}
 
// A utility function to check
// if size of heap is 1 or not
int isSizeOne(struct MinHeap* minHeap)
{
 
    return (minHeap->size == 1);
}
 
// A standard function to extract
// minimum value node from heap
struct MinHeapNode* extractMin(struct MinHeap* minHeap)
{
 
    struct MinHeapNode* temp = minHeap->array[0];
    minHeap->array[0] = minHeap->array[minHeap->size - 1];
 
    --minHeap->size;
    minHeapify(minHeap, 0);
 
    return temp;
}
 
// A utility function to insert
// a new node to Min Heap
void insertMinHeap(struct MinHeap* minHeap,
                   struct MinHeapNode* minHeapNode)
{
 
    ++minHeap->size;
    int i = minHeap->size - 1;
 
    while (i
           && minHeapNode->freq
                  < minHeap->array[(i - 1) / 2]->freq) {
 
        minHeap->array[i] = minHeap->array[(i - 1) / 2];
        i = (i - 1) / 2;
    }
 
    minHeap->array[i] = minHeapNode;
}
 
// A standard function to build min heap
void buildMinHeap(struct MinHeap* minHeap)
{
 
    int n = minHeap->size - 1;
    int i;
 
    for (i = (n - 1) / 2; i >= 0; --i)
        minHeapify(minHeap, i);
}

// Utility function to check if this node is leaf
int isLeaf(struct MinHeapNode* root)
{
 
    return !(root->left) && !(root->right);
}
 
// Creates a min heap of capacity
// equal to size and inserts all character of
// data[] in min heap. Initially size of
// min heap is equal to capacity
struct MinHeap* createAndBuildMinHeap(char data[],
                                      int freq[], int size)
{
 
    struct MinHeap* minHeap = createMinHeap(size);
 
    for (int i = 0; i < size; ++i)
        minHeap->array[i] = newNode(data[i], freq[i]);
 
    minHeap->size = size;
    buildMinHeap(minHeap);
 
    return minHeap;
}
 
// The main function that builds Huffman tree
struct MinHeapNode* buildHuffmanTree(char data[],
                                     int freq[], int size)
{
    struct MinHeapNode *left, *right, *top;
 
    // Step 1: Create a min heap of capacity
    // equal to size.  Initially, there are
    // modes equal to size.
    struct MinHeap* minHeap
        = createAndBuildMinHeap(data, freq, size);
 
    // Iterate while size of heap doesn't become 1
    while (!isSizeOne(minHeap)) {
 
        // Step 2: Extract the two minimum
        // freq items from min heap
        left = extractMin(minHeap);
        right = extractMin(minHeap);
 
        // Step 3:  Create a new internal
        // node with frequency equal to the
        // sum of the two nodes frequencies.
        // Make the two extracted node as
        // left and right children of this new node.
        // Add this node to the min heap
        // '$' is a special value for internal nodes, not
        // used
        top = newNode('$', left->freq + right->freq);
 
        top->left = left;
        top->right = right;
 
        insertMinHeap(minHeap, top);
    }
 
    // Step 4: The remaining node is the
    // root node and the tree is complete.
    return extractMin(minHeap);
}

void printArr(int arr[], int n)
{
    char str[1][n];
    char conv[2];

    for (int i = 0; i < n; ++i){
        //sprintf(conv, "%d", arr[i]);
        //str[0][i] = conv;
    }

    //return str;
 
}
// Prints huffman codes from the root of Huffman Tree.
// It uses arr[] to store codes
int idx = 0;
void printCodes(struct MinHeapNode* root, int arr[],
                int top, char *letters, char binaryVal[][9])
{
    // pada awalnya, fungsi ini itu dari geeksforgeeks yang berguna untuk melakukan print setiap distinct letters dan juga kode biner hurufnya.
    // aku modifikasi biar huruf-huruf dan kode binernya disimpan didalam array.
 
    // jika jalur ke huruf ke kiri, tandai dengan 0
    if (root->left) { // traverse ke kiri dahulu
 
        arr[top] = 0;
        printCodes(root->left, arr, top + 1, letters, binaryVal); 
    }
 
    // jika jalur ke huruf ke kanan, tandai dengan 1
    if (root->right) { // traverse ke kanan
 
        arr[top] = 1;
        printCodes(root->right, arr, top + 1, letters, binaryVal);
    }
 
    if (isLeaf(root)) { // kalau sudah sampai leaf
        letters[idx] = root->data; // data huruf nya disimpan pada array letters.
        char conv[2]; // array char untuk convert kode biner yang masih dalam bentuk int ke char
        
        for (int i = 0; i < top; i++){ // untuk setiap digit pada kode biner huruf tersebut,
            sprintf(conv, "%d", arr[i]); // convert ke char
            binaryVal[idx][i] = conv[0]; // simpan ke array char 2D -> misal ingin akses kode biner untuk huruf index 0, maka akses binaryVal[0].
        }

        binaryVal[idx][top] = '\0'; // mark akhir array of char nya.
        idx++; // lanjut index huruf dan biner selanjutnya.
    }
    
}
 
// The main function that builds a
// Huffman Tree and print codes by traversing
// the built Huffman Tree
void HuffmanCodes(char data[], int freq[], int size)
{
    // Construct Huffman Tree
    struct MinHeapNode* root
        = buildHuffmanTree(data, freq, size);
 
    // Print Huffman codes using
    // the Huffman tree built above
    int arr[MAX_TREE_HT], top = 0;
 
    //printCodes(root, arr, top);
}

struct parentToChild {
    char string[1000];
    int strlen;
};

struct childToParent {
    char compressedString[1000][9];
    int strlen;
};

int main() 
{
	int fd1[2]; // Used to store two ends of first pipe 
	int fd2[2]; // Used to store two ends of second pipe 

	pid_t p; 

	if (pipe(fd1)==-1) 
	{ 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 
	if (pipe(fd2)==-1) 
	{ 
		fprintf(stderr, "Pipe Failed" ); 
		return 1; 
	} 

	p = fork(); 

	if (p < 0) 
	{ 
		fprintf(stderr, "fork Failed" ); 
		return 1; 
	} 

	// Parent process 
	else if (p > 0) 
	{ 
        // yang akan di read
        char compressedLetter[24];
        char binaryVal[24][9];
        struct childToParent batch2;

        // yang di write
        char letters[24]; // pada huffman, diperlukan array dari distinct characters pada kalimat,
		int occur[24]; // dan juga berapa kali huruf tersebut muncul.
        struct parentToChild batch; // struct yg nanti dikirim ke child melalui pipe.

		close(fd1[0]); // Close reading end of first pipe 

        // Section Soal Poin A

        FILE *fpin;
        char input[10] = "file.txt"; // nama file yang telah di download

        if((fpin = fopen(input, "r")) == NULL){
            printf("Unable to open");
            exit(0);
        }

        char string[1000]; // menyimpan kalimat yang ada di file
        char ch = getc(fpin); // ambil karakter pertama kalimat di dalam file
        while ((ch!=EOF)){ // belum sampai endfile
            if(((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || ch == ' ')){ // Berdasarkan soal, karakter selain huruf tidak masuk ke perhitungan
                char save[2]; // simpan karakter pada array of string, karena nanti mau di strcat ke variabel string.
                save[0] = toupper(ch); // Berdasarkan soal, ubah semua huruf ke kapital
                strncat(string, save, 1); // tempel char tersebut ke variabel string.
            }
            ch = getc(fpin); // lanjut karakter berikutnya.
        }

        strcpy(batch.string, string); // backup string nya, karena setelah ini hanya akan diambil distinct lettersnya -> proses kompresi huffman
        batch.strlen = strlen(string); // simpan length nya

        // method perhitungan frekuensi

        int count = 0; // untuk menghitung banyak huruf yang muncul pada huruf tertentu.
        long len = strlen(string);
    
		int temp = 0; // membantu indexing
		for (int i = 0; i < len; i++){
			count = 1; // starting point huruf sudah muncul sekali.
			if (string[i] != '\0'){ // jika karakter bukan \0 -> semacam marking huruf
				for (int j = i+1; j < len; j++){
					if (string[i] == string[j]){ // jika karakter awal = yang sedang diperiksa 
						count++;
						string[j] = '\0'; // mark huruf dengan \0, sehingga pada pengecekan huruf berikutnya, huruf ini langsung di skip.
					}
				}
				
				letters[temp] = string[i]; // simpan distinct letters ke array letters
				occur[temp] = count; // simpan berapa kali huruf tersebut muncul ke array occur
				temp++; // increment index
			}
		}
		
		int n = sizeof(letters) / sizeof(letters[0]); // panjang array letters dan occur

        // proses pengiriman ke child melalui pipe
		write(fd1[1], &n, sizeof(int));
		write(fd1[1], letters, sizeof(char)*n); 
		write(fd1[1], occur, sizeof(int)*n);
        write(fd1[1], &batch, sizeof(struct parentToChild));
        
		close(fd1[1]); 
        
		// Wait for child to send a string 
		wait(NULL); 

		close(fd2[1]); // Close writing end of second pipe 

        // Section Soal Poin D dan E

		read(fd2[0], &compressedLetter, sizeof(char)*n);
        read(fd2[0], &binaryVal, sizeof(char)*n*9);
        read(fd2[0], &batch2, sizeof(struct childToParent));
        
        int bitBefore = 0, bitAfter = 0;
        char decompressed[batch2.strlen]; // menyimpan string yang akan di decompress

        for (int i = 0; i < batch2.strlen; i++){
            char binary[9]; // menyimpan kode biner untuk tiap huruf
            strcpy(binary, batch2.compressedString[i]); // copy biner untuk huruf index ke i ke variabel
            for (int j = 0; j < 24; j++){
                if (strcmp(binary, binaryVal[j]) == 0){ // compare biner huruf yang sedang di cek dengan biner-biner yang ada di array
                    decompressed[i] = compressedLetter[j]; // set string index ke-i dengan huruf yang sesuai dengan binernya.
                    bitAfter += strlen(binaryVal[j]); // banyak digit binernya / panjang string nya ditambahkan ke variabel bitAfter
                }
            }
        }

        bitBefore = strlen(decompressed) * 8; // bit pada setiap huruf alfabet bernilai 8.
        
        // print perbandingan string dan bit nya.
        printf("\n%s", decompressed);
        printf("\nBit sebelum kompresi: %d\n", bitBefore);

        printf("\n");
        for (int i = 0; i<batch.strlen; i++){
            printf("%s ", batch2.compressedString[i]); // kode biner setiap huruf (dalam bentuk string).
        } 
        printf("\nBit setelah kompresi: %d\n", bitAfter);

		close(fd2[0]); 
	} 

	// child process 
	else
	{ 
		close(fd1[1]); // Close writing end of first pipe 

        // yang di read
		char letters[24];
		int occur[24];
		int n;
        struct parentToChild batch;

        // yang di write
        char compressedLetter[24]; // distinct letters pada kalimat
        char binaryVal[24][9]; // kode biner pada setiap hurufnya
        struct childToParent batch2; // struct untuk menyimpan string yang telah di compress

        // Section Soal Poin B dan C

        // huffman tree
        struct MinHeapNode* root;
        int arr[MAX_TREE_HT], top = 0;

		read(fd1[0], &n, sizeof(int)); 
		read(fd1[0], &letters, sizeof(char)*n); 
		read(fd1[0], &occur, sizeof(int)*n); 
        read(fd1[0], &batch, sizeof(struct parentToChild));

        close(fd1[0]);
        close(fd2[0]);

        for (int i = 0; i < n; i++){
            printf("\n%c : %d\n", letters[i], occur[i]);
        }
        
        root = buildHuffmanTree(letters, occur, n); // buat tree huffman dengan mengirim array yang diterima dari parent.
        printCodes(root, arr, top, compressedLetter, binaryVal); // simpan tree tersebut dalam bentuk array, agar bisa dikirim melalui pipe.
       
        // dari array huffman yang dimiliki, periksa setiap huruf pada kalimat di file, ubah ke kode binary.
        for (int i = 0; i < batch.strlen; i++){
            char cek = batch.string[i]; // ambil index ke i, simpan di char.
            for (int j = 0; j < n; j++){ // pengecekan array huffman
                if (cek == compressedLetter[j]){ // jika huruf sama dengan distinct letter pada array
                    //printf("%s\n", binaryVal[j]);
                    strcpy(batch2.compressedString[i], binaryVal[j]); // ambil kode binernya, copy ke compressed string.
                }
            }
        }

        batch2.strlen = batch.strlen; // atur panjang string nya sama dengan string awal.
        
        for (int i = 0; i < n; i++){
            printf("%c : %s\n", compressedLetter[i], binaryVal[i]);
        } 
        
        // kirim distinct letters, kode binernya, dan compressed string ke parent.
        write(fd2[1], compressedLetter, sizeof(char)*n);
        write(fd2[1], binaryVal, sizeof(char)*n*9);
        write(fd2[1], &batch2, sizeof(struct childToParent));
        close(fd2[1]);
        
		exit(0); 
	} 
}