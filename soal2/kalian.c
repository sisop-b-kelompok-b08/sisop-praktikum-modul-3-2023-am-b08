#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define ROW1 4   
#define COL1 2   
#define ROW2 2   
#define COL2 5   
#define SHM_SIZE 1024  

void multiply_matrices(unsigned long long mat1[][COL1], unsigned long long mat2[][COL2], unsigned long long result[][COL2]) {
    int i, j, k;
    for (i = 0; i < ROW1; i++) {     
        for (j = 0; j < COL2; j++) { 
            result[i][j] = 0;       
            for (k = 0; k < ROW2; k++) { 
                result[i][j] += mat1[i][k] * mat2[k][j]; 
            }
        }
    }
}

void print_matrix(unsigned long long mat[][COL2], int row, int col) {
    int i, j;
    for (i = 0; i < row; i++) {     
        for (j = 0; j < col; j++) { 
            printf("%llu ", mat[i][j]); 
        }
        printf("\n"); 
    }
}


int main() {
    unsigned long long mat1[ROW1][COL1], mat2[ROW2][COL2], result[ROW1][COL2];
    int i, j;
    // key 1 for cinta.c
    key_t key1 = 1234; 
    // key 2 for sisop.c
    key_t key2 = 5678; 

    // Initialize random seed
    srand(time(NULL));

    // Generate random values for mat1
    printf("Matrix 1:\n");
    for (i = 0; i < ROW1; i++) {
        for (j = 0; j < COL1; j++) {
            mat1[i][j] = rand() % 5 + 1;
            printf("%llu ", mat1[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    // Generate random values for mat2
    printf("Matrix 2:\n");
    for (i = 0; i < ROW2; i++) {
        for (j = 0; j < COL2; j++) {
            mat2[i][j] = rand() % 4 + 1;
            printf("%llu ", mat2[i][j]);
        }
        printf("\n");
    }

    printf("\n");

    // Perform matrix multiplication
    multiply_matrices(mat1, mat2, result);

    // Print the resulting matrix
    printf("Result of matrix multiplication:\n");
    print_matrix(result, ROW1, COL2);

    // create shared memory
    int shmid = shmget(key1, SHM_SIZE, 0666 | IPC_CREAT);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    // create shared memory key2
    int shmid2 = shmget(key2, SHM_SIZE, 0666 | IPC_CREAT);
    if (shmid2 < 0) {
        perror("shmget");
        exit(1);
}

    // attach shared memory
    unsigned long long *shm_ptr1 = (unsigned long long *)shmat(shmid, NULL, 0);
    if ((intptr_t)shm_ptr1 == -1) {
        perror("shmat");
        exit(1);
    }

    // attach shared memory key2
    unsigned long long *shm_ptr2 = (unsigned long long *)shmat(shmid2, NULL, 0);
    if ((intptr_t)shm_ptr2 == -1) {
        perror("shmat");
        exit(1);
}

    // copy result matrix to shared memory
    unsigned long long *result_ptr = &result[0][0];
    for (i = 0; i < ROW1 * COL2; i++) {
        shm_ptr1[i] = result_ptr[i];
        shm_ptr2[i] = result_ptr[i];
    }

printf("Result matrix has been written to shared memory with keys %d and %d\n", key1, key2);

    // wait for 10 seconds
    sleep(10);

    // detach shared memory
    if (shmdt(shm_ptr1) == -1) {
        perror("shmdt");
        exit(1);
    }
    // detach shared memory
    if (shmdt(shm_ptr2) == -1) {
        perror("shmdt");
        exit(1);
    }
    return 0;
}

