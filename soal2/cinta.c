#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW 4
#define COL 5
#define SHM_SIZE 1024

struct args {
    int row;
    int col;
};

unsigned long long int (*result)[5];

void *factorial(void *arguments) {
    int row = ((struct args *) arguments)->row;
    int col = ((struct args *) arguments)->col;
    int num = result[row][col];
    unsigned long long int fac = 1;
    for (int i = 1; i <= num; i++) {
        fac *= i;
    }
    result[row][col] = fac;
    pthread_exit(NULL);
}

void attach_shared_mem() {
    int shmid;
    key_t key = 1234;

    // attach shared memory segment
    if ((shmid = shmget(key, SHM_SIZE, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    if ((result = shmat(shmid, NULL, 0)) == (unsigned long long int (*)[5]) -1) {
        perror("shmat");
        exit(1);
    }
}

void print_matrix(unsigned long long int matrix[][COL], char* message) {
    printf("%s\n", message);
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%llu ", matrix[i][j]);
        }
        printf("\n");
    }
}

void calculate_factorial(pthread_t tid[][COL], struct args thread_args[][COL]) {
    // calculate factorial of each element using threads
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 5; j++) {
            thread_args[i][j].row = i;
            thread_args[i][j].col = j;
            pthread_create(&tid[i][j], NULL, factorial, (void *) &thread_args[i][j]);
        }
    }

    // wait for threads to finish
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            pthread_join(tid[i][j], NULL);
        }
    }
}

void detach_shared_mem() {
    // detach shared memory segment
    shmdt(result);
}

int main() {
    pthread_t tid[4][5];
    struct args thread_args[4][5];
    clock_t start, end;
    double cpu_time_used;

    // attach shared memory segment
    attach_shared_mem();

    print_matrix(result, "Hasil Perkalian Matriks:");

    // calculate factorial of each element using threads
    start = clock();
    calculate_factorial(tid, thread_args);
    end = clock();

    // calculate time taken
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    print_matrix(result, "Hasil Faktorial Matriks:");

    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", cpu_time_used);

    // detach shared memory segment
    detach_shared_mem();

    return 0;
}
