#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define ROW 4
#define COL 5
#define SHM_SIZE 1024

unsigned long long int (*result)[COL];

void attach_shared_mem() {
    int shmid;
    key_t key = 5678;

    // attach shared memory segment
    if ((shmid = shmget(key, SHM_SIZE, 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    if ((result = shmat(shmid, NULL, 0)) == (unsigned long long int (*)[COL]) -1) {
        perror("shmat");
        exit(1);
    }
}

void print_matrix(unsigned long long int matrix[][COL], const char* message) {
    printf("%s:\n", message);
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            printf("%llu ", matrix[i][j]);
        }
        printf("\n");
    }
}

void calculate_factorial() {
    // calculate factorial of each element
    for (int i = 0; i < ROW; i++) {
        for (int j = 0; j < COL; j++) {
            int num = result[i][j];
            unsigned long long int fac = 1;
            for (int k = 1; k <= num; k++) {
                fac *= k;
            }
            result[i][j] = fac;
        }
    }
}

void detach_shared_mem() {
    // detach shared memory segment
    shmdt(result);
}

int main() {
    clock_t start, end;
    double cpu_time_used;

    // attach shared memory segment
    attach_shared_mem();

    print_matrix(result, "Hasil Perkalian Matriks");

    // calculate factorial of each element
    start = clock();
    calculate_factorial();
    end = clock();

    // calculate time taken
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    print_matrix(result, "Hasil Faktorial Matriks");

    printf("\nWaktu yang dibutuhkan untuk menghitung faktorial setiap elemen matriks: %f detik\n", cpu_time_used);

    // detach shared memory segment
    detach_shared_mem();

    return 0;
}
