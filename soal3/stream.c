#define _GNU_SOURCE

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <regex.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <json-c/json.h> // sudo apt install libjson-c-dev
#include <semaphore.h>

struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[1024];
} message;

int matching_line(char *fname, char *str){ // return banyaknya baris pada file yang mengandung str
    FILE *fp;

    if((fp = fopen(fname, "r")) == NULL){ 
        return(-1);
    }

    int count = 0;
    char save[1024];
    while(fgets(save, sizeof(save), fp) != NULL){
        // set index akhir save menjadi akhir array \0 jika ditemukan newline
        if((strlen(save) > 0) && (save[strlen(save) - 1] == '\n')) save[strlen(save) - 1] = '\0'; 
        if((strcasestr(save, str)) != NULL) count++; // cari string str yang muncul di string save, ignore case sensitive.
    }

    if(fp){
        fclose(fp);
    }

    return count;
}

int song_exist(char *fname, char *str){  // cari lagu str apakah sudah ada di fname
    FILE *fp;
    
    if((fp = fopen(fname, "r")) == NULL){ 
        return(-1);
    }

    int count = 0;
    char save[1024];
    while(fgets(save, sizeof(save), fp) != NULL){ 
        if((strlen(save) > 0) && (save[strlen(save) - 1] == '\n')) save[strlen(save) - 1] = '\0';
        if((!strcasecmp(save, str))) count++; // jika ditemukan maka increment count
    }

    if(fp){
        fclose(fp);
    }

    return count;
}

void decode_base64(char* str) { // proses decode judul lagu dengan format base64
    
    char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                            'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                            'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                            'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

    char count = 0;
    int idx = 0;
    char buf[4];
    char* res = malloc(strlen(str) * 3 / 4 + 1);
    for(int i = 0; str[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && encoding_table[k] != str[i]; k++);
        buf[count++] = k;
        if(count == 4) {
            res[idx++] = (buf[0] << 2) + (buf[1] >> 4);
            if(buf[2] != 64) res[idx++] = (buf[1] << 4) + (buf[2] >> 2);
            if(buf[3] != 64) res[idx++] = (buf[2] << 6) + buf[3];
            count = 0;
        }
    }

    res[idx] = '\0';   
    strcpy(str, res); // copy result ke string awal.
}

void decode_rot13(char *str){ // proses decode judul lagu dengan format rot13
    char c;
    while(*str){
        c = *str;
        if(isalpha(c)){ 
            if(c >= 'a' && c <= 'm' || c >= 'A' && c <= 'M')*str += 13; // tambah 13 jika char adalah huruf antara a-m
            else *str -= 13; // kurang 13 jika char huruf antara n-z
        } str++;
    }
}

void decode_hex(char *str){ // proses decode judul lagu dengan format hex
    char string[1024];
    int len = strlen(str);
    for(int i = 0, j = 0; j < len; ++i, j += 2){ 
        int val[1];
        sscanf(str + j, "%2x", val);
        string[i] = val[0];
        string[i + 1] = '\0';
    }
    strcpy(str, string);
}

void sort(){
    char sort_comm[50];
    strcpy(sort_comm, "sort -u playlist.txt -o playlist.txt"); 
    system(sort_comm);
}

void decrypt(){
    const char *fname = "song-playlist.json";
    struct json_object *parsed;
    parsed = json_object_from_file(fname); 

    if(!json_object_is_type(parsed, json_type_array)){ // cek tipe json -> array
        printf("Array not Found!\n");
        return;
    }

    int len = json_object_array_length(parsed);
    for(int i = 0; i < len; i++){

        struct json_object *obj = json_object_array_get_idx(parsed, i); // ambil setiap object json berdasarkan i
    
        if(!json_object_is_type(obj, json_type_object)){  // cek tipe json -> object
            printf("Item not Found!\n");
            continue;
        }

        struct json_object *method, *song;
        if(!json_object_object_get_ex(obj, "method", &method)){ // cari method nya berupa apa
            printf("Encryption Not Found!\n");
            continue;
        }
        if(!json_object_object_get_ex(obj, "song", &song)){ // cari judul lagu nya
            printf("Song Not Found!\n");
            continue;
        }

        char judulLagu[json_object_get_string_len(song) + 1]; // simpan object json pada arr of char
        strcpy(judulLagu, json_object_get_string(song));

        // panggil fungsi decode untuk masing-masing method
        if(strstr("rot13",json_object_get_string(method))) decode_rot13(judulLagu); 
        else if(strstr("hex", json_object_get_string(method))) decode_hex(judulLagu);
        else if(strstr("base64", json_object_get_string(method))) decode_base64(judulLagu); 
        
        char write_comm[strlen(judulLagu) + 30];
        snprintf(write_comm, sizeof(write_comm), "echo \"%s\" >> playlist.txt", judulLagu); 
        system(write_comm);
        printf("After Decrypt:\nMethod: %s\nSong: %s\n\n", json_object_get_string(method), judulLagu);
    }
    
    json_object_put(parsed); // decrement reference dari json sampai free
    sort();  // sort berdasarkan alfabet
}

void addSong(int uid, char *in){
    char txtname[] = "playlist.txt";
    char response[strlen(in)];
    strcpy(response, in);

    if(song_exist(txtname, response) < 1){ // cek jika lagu yang ingin ditambah sudah ada, <1 berarti belum ada
        char write_comm[strlen(response) + 30];
        snprintf(write_comm, sizeof(write_comm), "echo \"%s\" >> playlist.txt", response); 
        system(write_comm);
        sort();

        printf("USER \"%d\" ADD %s\n", uid, response);
    } else {
        printf("SONG ALREADY ON PLAYLIST\n");
    }
}

void play(int uid, char *in){
    char txtname[] = "playlist.txt";
    char response[strlen(in) + 1];
    strcpy(response, in);

    int song_count = 0;
    song_count = matching_line(txtname, response); // hitung berapa lagu yang mengandung str response

    if(song_count == 0){
        printf("THERE IS NO SONG CONTAINING \"%s\"\n", response);
    } 
    else if(song_count == 1){ // play lagu jika hanya ada 1 matching line
        FILE *fp;
        char save[1024];
        char title[1025];

        if((fp = fopen(txtname, "r")) == NULL){
            return;
        }

        while(fgets(save, sizeof(save), fp) != NULL){
            if((strlen(save) > 0) && (save[strlen(save) - 1] == '\n')) save[strlen(save) - 1] = '\0'; // 
            if((strcasestr(save, response)) != NULL) { // jika judul lagu response sesuai dengan yang ada di file.
                strcpy(title, save); // ambil dan simpan di title
            }
        }

        if(fp){
            fclose(fp);
        }
        
        printf("USER \"%d\" PLAYING %s\n", uid, title);
    } else { // play list lagu-lagu yang bersesuaian dengan response
        printf("THERE ARE \"%d\" SONGS CONTAINING \"%s\"\n", song_count, response);
        FILE *fp;
        int count = 1;
        char save[1024];

        if((fp = fopen(txtname, "r")) == NULL){
            return;
        }

        while(fgets(save, sizeof(save), fp) != NULL){
            if(strcasestr(save, response) != NULL){ // jika kata kunci response ditemukan pada suatu lagu di save
                printf("%d. %s", count, save);
                count++;
            }
        }

        if(fp){
            fclose(fp);
        }
    }

}

void listAllSongs(){
    sort();
    char cat_comm[50];
    strcpy(cat_comm, "cat playlist.txt");
    system(cat_comm);
    printf("\n");
}

int main(){
    regex_t decrypt_rgx, list_rgx, play_rgx, add_rgx; // list regular expression command user
    
    // pattern regular expression untuk tiap command
    const char *decrypt_ptrn = "^DECRYPT\n$"; 
    const char *list_ptrn = "^LIST\n$"; 
    const char *play_ptrn = "^PLAY (.+)\n$"; 
    const char *add_ptrn = "^ADD (.+)\n$"; 
    
    int ret = regcomp(&decrypt_rgx, decrypt_ptrn, REG_EXTENDED);
    ret |= regcomp(&list_rgx, list_ptrn, REG_EXTENDED);
    ret |= regcomp(&play_rgx, play_ptrn, REG_EXTENDED);
    ret |= regcomp(&add_rgx, add_ptrn, REG_EXTENDED);
    
    key_t key;
    int msgid;
    key = ftok("stream_playlist", 50);
  
    sem_unlink("/user"); // unlink semaphore user
    sem_unlink("/stream"); // unlink semaphore stream

    sem_t *sem_stream = sem_open("/stream", O_CREAT | O_EXCL, 0660, 0); // inisialisasi sem_stream ke 0
    if(sem_stream == SEM_FAILED){
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }
    
    sem_t *sem_user = sem_open("/user", O_CREAT | O_EXCL, 0660, 2); // inisialisasi sem_user ke 2
    if(sem_user == SEM_FAILED){
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
    
    msgid = msgget(key, 0666 | IPC_CREAT);
    
    while(1){
        
        sem_wait(sem_stream); // tunggu user mengirim pesan

        msgrcv(msgid, &message, sizeof(message), 1, 0); // receive pesan yang dikirim user

        printf("USER \"%d\" SENT DATA %s\n", message.user_pid, message.mesg_text); // print detail pesan user

        regmatch_t match[2];
        
        ret = regexec(&decrypt_rgx, message.mesg_text, 0, NULL, 0); // set ret awal untuk decrypt

        if(ret == 0){  // jika user meminta decrypt
            decrypt(); // panggil fungsi decrypt
        } else { // jika bukan decrypt
            ret = regexec(&list_rgx, message.mesg_text, 0, NULL, 0); // set ret menjadi list
            if(ret == 0) { 
                listAllSongs(); // panggil fungsi listAllSongs
            } else { // Jika bukan list
                ret = regexec(&add_rgx, message.mesg_text, 2, match, 0); // set ret menjadi add
                if(ret == 0){
                    char addArg[match[1].rm_eo - match[1].rm_so + 1]; // ambil argumen add dari user
                    memcpy(addArg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                    addArg[match[1].rm_eo - match[1].rm_so] = '\0'; // set akhir string \0
                    addSong(message.user_pid, addArg);
                } else { // jika bukan add
                    ret = regexec(&play_rgx, message.mesg_text, 2, match, 0); // set ret menjadi play
                    if(ret == 0){
                        char playArg[match[1].rm_eo - match[1].rm_so + 1]; // ambil argumen play dari user
                        memcpy(playArg, message.mesg_text + match[1].rm_so, match[1].rm_eo - match[1].rm_so);
                        playArg[match[1].rm_eo - match[1].rm_so] = '\0';
                        play(message.user_pid, playArg);
                    } else { // jika command tidak valid
                        printf("UNKNOWN COMMAND!\n");
                    }
                }
            }
        }
        
        sem_post(sem_user); // post signal semaphore ke user
    }
    return 0;
}