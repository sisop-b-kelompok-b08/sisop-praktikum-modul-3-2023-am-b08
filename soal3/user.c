#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define MAX 1024

struct mesg_buffer {
    long mesg_type;
    int user_pid;
    char mesg_text[MAX];
} message;

void create_message_queue(int *msgid) {
    key_t key = ftok("stream_playlist", 50);
    *msgid = msgget(key, 0666 | IPC_CREAT);
    message.mesg_type = 1;
    message.user_pid = getpid();
}

void open_semaphores(sem_t **sem_stream, sem_t **sem_user) {
    *sem_stream = sem_open("/stream", 0);
    if (*sem_stream == SEM_FAILED) {
        perror("sem_open/stream");
        exit(EXIT_FAILURE);
    }

    *sem_user = sem_open("/user", 0);
    if (*sem_user == SEM_FAILED) {
        perror("sem_open/user");
        exit(EXIT_FAILURE);
    }
}

int check_semaphore_value(sem_t *sem) {
    int sem_val;
    sem_getvalue(sem, &sem_val);

    return sem_val;
}

void wait_semaphore(sem_t *sem) {
    sem_wait(sem);
}

void post_semaphore(sem_t *sem) {
    sem_post(sem);
}

void send_message(int msgid) {
    msgsnd(msgid, &message, sizeof(message), 0);
}

void read_input() {
    printf("Insert Data:");
    fgets(message.mesg_text, MAX, stdin);
}

int main() {
    int msgid;
    sem_t *sem_stream, *sem_user;

    create_message_queue(&msgid);
    open_semaphores(&sem_stream, &sem_user);

    if (check_semaphore_value(sem_user) == 0) {
        printf("STREAM SYSTEM OVERLOAD\n");
        return 1;
    }

    while(1) {
        printf("Waiting...\n");
        wait_semaphore(sem_user);
        read_input();

        if(strcmp("EXIT\n", message.mesg_text) == 0) {
            post_semaphore(sem_user);
            return 0;
        }

        send_message(msgid);
        printf("Data sent: %s\n", message.mesg_text);

        post_semaphore(sem_stream);
    }

    return 0;
}
